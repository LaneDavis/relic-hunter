﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedLife : MonoBehaviour {

    public float Duration;
    private float timeOfDeath;

    private void Awake() {
        timeOfDeath = Time.time + Duration;
    }

    private void Update() {
        if (Time.time > timeOfDeath) {
            Destroy(gameObject);
        }
    }

}
