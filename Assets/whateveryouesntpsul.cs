﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class whateveryouesntpsul : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() {
        Player Steeve = Rewired.ReInput.players.GetPlayer(0);
        float moveX = Steeve.GetAxisRaw("Move X");
        float moveZ = Steeve.GetAxisRaw("Move Z");
        Vector3 moveVector = new Vector3(moveX, 0f, moveZ);
        transform.Translate(moveVector * Time.deltaTime);
    }
}
