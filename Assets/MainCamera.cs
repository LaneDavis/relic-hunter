﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MainCamera : MonoBehaviour {

    // Primary — The Camera attached to this object.
    private static Camera attachedCamera;
    private static GameObject cameraObject;
    
    // Secondary — If this object is destroyed, cache the current main camera.
    private static readonly Camera _fallbackMainCamera = null;
    private static Camera FallbackMainCamera { get {
            if (_fallbackMainCamera == null) return Camera.main;
            return _fallbackMainCamera;
        } }

    public static Camera Camera => attachedCamera ? attachedCamera : FallbackMainCamera;
    public static GameObject GameObject => cameraObject ? cameraObject : FallbackMainCamera.gameObject;
    public static Transform Transform => GameObject.transform;
    
    private void Awake() {
        attachedCamera = GetComponent<Camera>();
        cameraObject = gameObject;
    }
    
    public static Vector2 Cam2World(Vector2 inputVec) {
        return GeometryUtils.RotateVector(inputVec, -Transform.rotation.eulerAngles.y);
    }
    
    public static Vector2 World2Cam(Vector2 inputVec) {
        return GeometryUtils.RotateVector(inputVec, Transform.rotation.eulerAngles.y);
    }

}
