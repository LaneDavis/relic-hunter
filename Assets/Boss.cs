﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    [HideInInspector] public PlayerData PlayerData;
    public float Amount = 1f;
    public float Duration = 1f;
    private float shakeEndTime;
    
    
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Boss HAS SPAWNED");
        Amount = Mathf.Clamp01(Amount);

        shakeEndTime = Time.time + Duration;
       
    }
   
    // Update is called once per frame
    void Update()
    {
        if (Time.time < shakeEndTime) {
            PlayerManager.Instance.AllPlayers().ForEach(e => e.AddRumble(Amount));
        }   
    }
}
