﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(BuffController))]
public class stupidzombieinput : MonoBehaviour, IEnemyInput { 

 public NavMeshAgent NavMeshAgent;

 public EntityController EntityController;
 private BuffController buffController;
 private float baseSpeed;

 void Start() {
  baseSpeed = NavMeshAgent.speed;
  buffController = GetComponent<BuffController>();
 }

//  Update is called once per frame
 void Update() {

  PlayerData nearestPlayer = PlayerManager.Instance.NearestPlayerToPoint(transform.position);
  if (nearestPlayer != null) {
   Vector3 aimVec = (nearestPlayer.PlayerObject.transform.position - transform.position).normalized;
   EntityController.ReleaseAttack(new Vector2(aimVec.x, aimVec.z));
  }
  
  // Set Speed Based on Buffs
  float speedBuffValue = buffController.MoveSpeedBuffs.Value;
  NavMeshAgent.speed = baseSpeed * speedBuffValue;
  GetComponent<Rigidbody>().velocity *= (1f - speedBuffValue * Time.deltaTime);

  if (!NavMeshAgent.isOnNavMesh) return;
  PlayerData playerToChase = PlayerManager.Instance.NearestPlayerToPoint(transform.position);

  if (playerToChase != null && Vector3.Distance(playerToChase.PlayerObject.transform.position, transform.position) < 50f)
  {
//  do movement behavior here.
      Vector3 target = playerToChase.PlayerObject.transform.position;
      Vector3 vecToTarget = target - transform.position;
      NavMeshAgent.SetDestination(target);
  }
 else
 {
 // do nothing, or whatever you want when no players exist.

 }


 }

 public void WarpToMesh(Vector3 point) {
  GetComponent<NavMeshAgent>().Warp(point);
 }
 
}
