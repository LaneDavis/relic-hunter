﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimations : MonoBehaviour {

    public GameObject FxBlendingObject;
    
    public FxController JumpFX;
    public List<FxController> TakeDamageFX;


    private void Start() {
        
    }
    
    public void OnJump () {
        JumpFX.Trigger();
    }

    public void OnTakeDamage(float amplitude = 1f) {
        TakeDamageFX.ForEach(e => e.TriggerAll(amplitude));
    }
    
}
