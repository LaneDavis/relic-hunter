﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;

    
public class ReplaceWithPrefabs : MonoBehaviour {

    public string DirectoryToSearch = "Assets/Bitgem/Cube_World/Prefabs";
    public string PrefabExtension = ".prefab";
    
    public void Replace() {

        List<Transform> children = transform.GetComponentsInChildren<Transform>().Where(e => e.parent == transform).ToList();

        for (int i = children.Count - 1; i >= 0; i--) {
            string prefabPath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(children[i].gameObject);
            string assetName = Path.GetFileNameWithoutExtension(prefabPath);
            string fullString = DirectoryToSearch + "/" + assetName + PrefabExtension;
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(fullString);

            if (prefab != null) {
                GameObject newObj = (GameObject) PrefabUtility.InstantiatePrefab(prefab);
                newObj.transform.SetParent(transform);
                newObj.transform.position = children[i].position;
                newObj.transform.rotation = children[i].rotation;
                newObj.transform.localScale = children[i].transform.localScale;
                DestroyImmediate(children[i].gameObject);
            }
        }

    }
    
}

#if UNITY_EDITOR
[CustomEditor(typeof(ReplaceWithPrefabs))]
public class ReplaceWithPrefabsEditor : Editor {
    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        ReplaceWithPrefabs myScript = (ReplaceWithPrefabs) target;

        if (GUILayout.Button("Replace!")) {
            myScript.Replace();
        }
        
    }
}
#endif
