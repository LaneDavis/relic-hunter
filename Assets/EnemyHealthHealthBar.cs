﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthHealthBar : MonoBehaviour
{

    public float CurHealth;
    public float MaxHealth;

    public GameObject HealtBarUI;
    public Slider slider;


    

    [Header("Taking Damage")]
    public float DOT_TickTime = 0.5f;
    private float DOT_TickTimer;
    public float DOT_SafetyTime = 0.1f;
    private float DOT_SafetyTimer;


    private List<Collision> HazardCollisions = new List<Collision>();

    void Start()
    {

        CurHealth = MaxHealth;
        slider.value = CalculateHealth();


    }

   
    void Update()
    {


        UpdateDamageOverTime();

        if (transform.position.y < KillFloor.KillFloorHeight)
        {
            Die();
        }
        slider.value = CalculateHealth();
        if (CurHealth < MaxHealth)
        {

            HealtBarUI.SetActive(true);


        }
        if (CurHealth <= 0) 
        {

            Die();

        }

        if(CurHealth > MaxHealth)
        {

            CurHealth = MaxHealth;

        }

    }


    float CalculateHealth()
    {

        return CurHealth / MaxHealth;

    }



    public void TakeDamage(float damageAmount)
    {

        CurHealth -= damageAmount;
        Debug.Log("Taking Damage");
    }


    



    private void UpdateDamageOverTime()
    {

        if (DOT_TickTimer >= DOT_TickTime)
        {
            DOT_TickTimer = 0f;
        }

        if (HazardCollisions.Count > 0)
        {
            DOT_SafetyTimer = 0f;

            if (DOT_TickTimer <= 0f)
            {
                TakeDamage(20f);
            }

            DOT_TickTimer += Time.fixedDeltaTime;
        }
        else
        {
            DOT_SafetyTimer += Time.deltaTime;
            if (DOT_SafetyTimer >= DOT_SafetyTime)
            {
                DOT_TickTimer = 0f;
            }
        }

        HazardCollisions.Clear();

    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 11)
        {
            HazardCollisions.Add(collision);
        }
   
    }


    public void Die()
    {

        Destroy(gameObject);

    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.layer == 11)
        {
            HazardCollisions.Add(collision);
        }
        
    }



}
