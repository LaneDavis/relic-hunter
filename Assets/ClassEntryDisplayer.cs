﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ClassEntryDisplayer : MonoBehaviour {

    public TextMeshProUGUI Text;
    public Image Icon;
    public Image IconColor;
    public Image BackgroundColor;
    public Image Cover;

    public float CoverTargetOpacity => isHighlighted ? 0f : 0.5f;
    private float coverOpacCur = 0.5f;
    private float coverOpacVel;
    private float coverOpacTime = 0.15f;
    
    private ClassSelectionController selectionController;
    [HideInInspector] public PlayerBaseStats DisplayedClass;
    private Color RootColor => DisplayedClass == null ? Color.black : DisplayedClass.ClassColor;

    private bool isHighlighted;
    private bool initialized;
    
    public void Init(ClassSelectionController owner, PlayerBaseStats newClass) {
        if (initialized) return;
        initialized = true;
        selectionController = owner;
        DisplayedClass = newClass;

        Icon.sprite = newClass.ClassIcon;
        IconColor.color = RootColor;
        BackgroundColor.color = ColorUtilities.BlendColors(new List<ColorWeight>() {new ColorWeight(RootColor, 1f), new ColorWeight(Color.black, 2f)});
        Text.text = newClass.DisplayName;
        Text.color = Color.white;
    }

    public void Highlight() {
        isHighlighted = true;
    }

    public void UnHighlight() {
        isHighlighted = false;
    }

    private void Update() {
        if (!initialized || !selectionController.IsSelecting) return;
        coverOpacCur = Mathf.SmoothDamp(coverOpacCur, CoverTargetOpacity, ref coverOpacVel, coverOpacTime);
        Cover.color = new Color(0f, 0f, 0f, coverOpacCur);
    }

}
