﻿using UnityEngine;
using UnityEngine.Assertions;

public class SpriteFacingController : MonoBehaviour {

    private EntityController controller;

    public Sprite SpriteToward;
    public Sprite SpriteAway;
    public Sprite SpriteLeft;
    public Sprite SpriteRight;

    public SpriteRenderer SpriteRenderer;
    
    private void Awake() {
        
        // Ensure that the EntityController is set properly.
        controller = gameObject.GetComponentInAncestry<EntityController>();
        if (controller == null) { Debug.LogError("<b>SpriteFacingController</b> missing required EntityController in hierarchy."); }
        
        // Subscribe to the EntityController's OnMove Action. SetSprite will be called whenever the controller is told to move.
        controller.OnMove += SetSprite;
        
    }

    private void SetSprite(Vector2 moveVec) {
        
        // Convert back to the Camera Vector.
        Vector2 camVec = MainCamera.World2Cam(moveVec);
        
        // Set Sprite Based on Move Vector.
        if (camVec.magnitude < 0.01f) return;
        if (Mathf.Abs(camVec.x) > Mathf.Abs(camVec.y)) {
            SpriteRenderer.sprite = camVec.x > 0 ? SpriteRight : SpriteLeft;
        }
        else {
            SpriteRenderer.sprite = camVec.y > 0 ? SpriteAway : SpriteToward;
        }

    }
    
}
