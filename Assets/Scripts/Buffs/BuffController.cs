﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuffController : MonoBehaviour {
    
    public List<BuffTracker> BuffTrackers = new List<BuffTracker>();
    public BuffTracker MoveSpeedBuffs;


    private void Start() {
        MoveSpeedBuffs = new BuffTracker(Buff.BuffType.MoveSpeed);
        BuffTrackers.Add(MoveSpeedBuffs);
    }
    
    public void AddBuff(Buff newBuff) {
        BuffTracker buffTracker = BuffListWithType(newBuff.Type);
        
        // Check for Matching Buff
        foreach (Buff b in buffTracker.Buffs) {
            if (b.Name == newBuff.Name) {
                RefreshBuff(newBuff, b);
                return;
            }
        }
        
        // Otherwise, add it as a new buff.
        Buff buffCopy = new Buff();
        buffCopy.CopyBuff(newBuff);
        buffTracker.Buffs.Add(buffCopy);

    }

    private void Update() {

        // Establishing deltaTime in one place saves reference calls later.
        float dT = Time.deltaTime;
        
        // Loop through each type of buff.
        foreach (BuffTracker buffTracker in BuffTrackers) {
            buffTracker.Update(dT);
        }

    }
    
    private void RefreshBuff(Buff newBuff, Buff oldBuff) {
        oldBuff.CopyBuff(newBuff);
        oldBuff.Age = 0f;
    }
    
    private BuffTracker BuffListWithType(Buff.BuffType type) {
        switch (type) {
            case Buff.BuffType.MoveSpeed:
                return MoveSpeedBuffs;
            default:
                Debug.LogError("No matching buff list found", null);
                return null;
        }
    }

}

public class BuffTracker {

    public Buff.BuffType Type;
    public List<Buff> Buffs = new List<Buff>();
    public float Value = 0f;

    public BuffTracker(Buff.BuffType type) {
        Type = type;
    }

    public void Update(float dT) {
        
        // Increment Age.
        for (int i = 0; i < Buffs.Count; i++) {
            Buffs[i].Age += dT;
        }
            
        // Retire any buffs that have expired.
        for (int i = Buffs.Count - 1; i >= 0; i--) {
            if (Buffs[i].PctDurationLeft <= 0) {
                Buffs.RemoveAt(i);
            }
        }
        
        // Cache the value in case anything checks it this frame.
        Value = 1f;
        foreach (Buff b in Buffs.Where(e => e.FinalValue > 0f)) {
            Value += b.FinalValue;
        }

        foreach (Buff b in Buffs.Where(e => e.FinalValue < 0f)) {
            Value *= Mathf.Clamp01(1f + b.FinalValue);
        }

    }

}

[Serializable]
public class Buff {

    public enum BuffType {
        MoveSpeed
    }

    public string Name;
    public BuffType Type;
    public float Value;
    public float FinalValue => Fades ? Value * PctDurationLeft : Value;
    public float Duration;
    [HideInInspector] public float Age;
    public bool Fades;

    public float PctDurationSpent => Age / Duration;
    public float PctDurationLeft => 1f - PctDurationSpent;

    public void CopyBuff (Buff other) {
        Name = other.Name;
        Type = other.Type;
        Value = other.Value;
        Duration = other.Duration;
        Fades = other.Fades;
    }

}