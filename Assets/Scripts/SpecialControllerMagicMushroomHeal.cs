﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialControllerMagicMushroomHeal : SpecialController {
    
    public float HealAmount;

    // Release Special - Called on 1 Frame
    // This is when the special button is released.
    public override void ActivateSpecial(SpecialData data) {
        base.ActivateSpecial(data);
        PlayerData healedPlayer = PlayerManager.Instance.NearestPlayerToPoint(transform.position);

        healedPlayer.EntityData.CurHP = Mathf.Clamp(healedPlayer.EntityData.CurHP + HealAmount, 0f, healedPlayer.EntityData.MaxHP);
    }
    
}
