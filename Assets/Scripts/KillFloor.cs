﻿using UnityEngine;

public class KillFloor : MonoBehaviour {

    public float killFloorHeight = -6f;
    public static KillFloor Instance;
    public static float KillFloorHeight => Instance.killFloorHeight;

    private void Awake() {
        Instance = this;
    }
    
}
