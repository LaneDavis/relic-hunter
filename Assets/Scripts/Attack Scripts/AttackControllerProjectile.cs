﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Sirenix.OdinInspector.Editor;
using UnityEngine;

public class AttackControllerProjectile : AttackController {

    public GameObject ProjectilePrefab;

    [Header("Attack Stats")]
    [Range(0f, 100f)] public float FireRate = 6f;
    public bool AutoFire;

    [Header("Spread")]
    [Range(0f, 360f)] public float FanDegrees;
    public int ProjectilesPerAttack = 1;
    [Range(0f, 360f)] public float RandomSpread;

    [Header("Projectile Spawn Point")]
    public float SpawnHeight = 0.4f;
    public float SpawnLateral = 0.2f;
    
    // Firing Control
    private float timeOfNextShot;
    
    public override void HoldAttack(AttackData data) {
        base.HoldAttack(data);
        if (AutoFire && Time.time > timeOfNextShot) {
            ShootProjectile(data);
        }
    }
    
    public override void ReleaseAttack(AttackData data) {
        base.ReleaseAttack(data);
        if (Time.time > timeOfNextShot) {
            ShootProjectile(data);
        }
    }
    
    public virtual void ShootProjectile(AttackData data) {

        for (int i = 0; i < ProjectilesPerAttack; i++) {

            AttackData newData = new AttackData() {
                AimVector2D = MathUtilities.RotateVector2(data.AimVector2D, DegreeRotationForBulletNumber(i)),
                OwnerEntityData = data.OwnerEntityData,
                OwnerPlayerData = data.OwnerPlayerData
            };
            
            // Instantiate Projectile
            Vector3 Offset = new Vector3(data.AimVector2D.normalized.x * SpawnLateral, SpawnHeight, data.AimVector2D.normalized.y * SpawnLateral);
            GameObject newProjectile = Instantiate(ProjectilePrefab, transform.position + Offset, Quaternion.identity, null);

            // Setup Projectile Controller
            ProjectileController newController = newProjectile.GetComponent<ProjectileController>();
            if (newController != null) newController.Initialize(newData);

            // Cooldown
            timeOfNextShot = FireRate <= 0f ? Mathf.Infinity : Time.time + 1f / FireRate;

        }

    }

    private float DegreeRotationForBulletNumber(int bulletNumber) {
        
        // Calculate Degrees from ProjectilesPerAttack and FanDegrees
        float fanDegrees = 0f;
        if (ProjectilesPerAttack > 1) {
            fanDegrees = -0.5f * FanDegrees + FanDegrees * (bulletNumber - 1) / (ProjectilesPerAttack - 1);
        }
        
        // Calculate Degrees from RandomSpread
        float randomDegrees = 0.5f * Random.Range(-RandomSpread, RandomSpread);

        return fanDegrees + randomDegrees;

    }
    
}
