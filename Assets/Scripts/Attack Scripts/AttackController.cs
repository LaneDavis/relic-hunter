﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackController : MonoBehaviour {

    public enum AttackStep {
        Start,
        Hold,
        Release
    };
    
    public delegate void AttackDelegate (AttackData data);
    public event AttackDelegate OnStartAttack;
    public event AttackDelegate OnHoldAttack;
    public event AttackDelegate OnReleaseAttack;
    
    // Start Attack — Called on 1 Frame
    // For players, this is when the attack button is pressed.
    public virtual void StartAttack(AttackData data) {
        OnStartAttack?.Invoke(data);
    }

    // Hold Attack — Called Once per Frame
    // For Players, this is whenever the attack button is held.
    public virtual void HoldAttack(AttackData data) {
        OnHoldAttack?.Invoke(data);
    }

    // Release Attack - Called on 1 Frame
    // For Players, this is when the attack button is released.
    public virtual void ReleaseAttack(AttackData data) {
        OnReleaseAttack?.Invoke(data);
    }
    
}

public class AttackData {

    public PlayerData OwnerPlayerData;
    public EntityData OwnerEntityData;
    public Vector2 AimVector2D;

}
