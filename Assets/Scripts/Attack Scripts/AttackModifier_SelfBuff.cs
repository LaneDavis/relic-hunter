﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AttackController))]
public class AttackModifier_SelfBuff : MonoBehaviour {

    private AttackController attackController;
    public AttackController.AttackStep AttackStep;
    public Buff Buff;

    private void Awake() {
        attackController = GetComponent<AttackController>();
        switch (AttackStep) {
            case AttackController.AttackStep.Start:
                attackController.OnStartAttack += ApplyBuff;
                break;
            case AttackController.AttackStep.Hold:
                attackController.OnHoldAttack += ApplyBuff;
                break;
            case AttackController.AttackStep.Release:
                attackController.OnReleaseAttack += ApplyBuff;
                break;
        }
    }

    private void ApplyBuff(AttackData data) {
        data.OwnerEntityData.BuffController.AddBuff(Buff);
    }

}
