﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class ClassSelectionController : MonoBehaviour {
    
    public static ClassSelectionController[] ClassSelectionControllers = new ClassSelectionController[4];
    public int PlayerNumber;
    
    private bool initialized;
    private PlayerData playerData;
    private List<PlayerBaseStats> ClassList;

    [HideInInspector] public bool IsSelecting;
    private bool wasSelecting;

    public GameObject EntryDisplayerTemplate;
    private List<ClassEntryDisplayer> classEntryDisplayers = new List<ClassEntryDisplayer>();
    private float timeOfNextTransition;
    private ClassEntryDisplayer highlightedDisplayer;
    private int highlightedIdx = 0;

    public RectTransform ListHolderRT;
    public RectTransform ClassListRT;
    
    [Header("Masker")]
    public RectTransform MaskerRT;
    public float MaskerExpandTime;
    private float maskerVelocity;
    private float maskerCurMag;
    
    public FxController NoticePulseFX;

    [Header("Canvas Group")]
    public CanvasGroup CanvasGroup;
    private const float alphaTime = .25f;
    private float alphaVelocity;
    private float alphaCur;

    private void Awake() {
        ClassSelectionControllers[PlayerNumber] = this;
    }

    public void Init(PlayerData newPlayerData, List<PlayerBaseStats> classList) {
        if (initialized) return;
        initialized = true;
        playerData = newPlayerData;
        ClassList = classList.ToList();
        
        foreach (PlayerBaseStats playerClass in ClassList) {
            GameObject newDisplayerObject = Instantiate(EntryDisplayerTemplate, EntryDisplayerTemplate.transform.parent);
            ClassEntryDisplayer newDisplayer = newDisplayerObject.GetComponent<ClassEntryDisplayer>();
            newDisplayer.Init(this, playerClass);
            classEntryDisplayers.Add(newDisplayer);
        }
        EntryDisplayerTemplate.SetActive(false);
        highlightedDisplayer = classEntryDisplayers[0];
        highlightedIdx = 0;
        highlightedDisplayer.Highlight();
        
    }

    public void StartClassSelection() {
        IsSelecting = true;
    }

    private void Update() {

        if (!initialized) return;
        
        if (IsSelecting != wasSelecting) {
            NoticePulseFX.Trigger();
        }

        if (IsSelecting) {
            float vertInput = playerData.RewiredPlayer.GetAxisRaw("Move Depth");
            if (vertInput > 0.5f && Time.time > timeOfNextTransition) {
                MoveUp();
            } else if (vertInput < -0.5f && Time.time > timeOfNextTransition) {
                MoveDown();
            }

            if (playerData.RewiredPlayer.GetButtonDown("Attack") || playerData.RewiredPlayer.GetButtonDown("Jump")) {
                IsSelecting = false;
                PlayerManager.Instance.SpawnPlayer(playerData, highlightedDisplayer.DisplayedClass);
            }

            Vector2 diff = ListHolderRT.position - highlightedDisplayer.transform.position;
            ClassListRT.transform.Translate(diff * 10f * Mathf.Clamp(Time.deltaTime, 0f, 0.1f));

        }
        
        // Resize Masker
        maskerCurMag = Mathf.SmoothDamp(maskerCurMag, IsSelecting ? 1f : 0.001f, ref maskerVelocity, MaskerExpandTime);
        MaskerRT.localScale = new Vector3(maskerCurMag, maskerCurMag, maskerCurMag);

        // Adjust Alpha
        alphaCur = IsSelecting ? 1f : Mathf.SmoothDamp(alphaCur, 0f, ref alphaVelocity, alphaTime);
        CanvasGroup.alpha = alphaCur;
        
        wasSelecting = IsSelecting;

    }

    private void MoveUp() {
        if (highlightedIdx <= 0) return;
        highlightedDisplayer.UnHighlight();
        highlightedIdx--;
        highlightedDisplayer = classEntryDisplayers[highlightedIdx];
        highlightedDisplayer.Highlight();
        timeOfNextTransition = Time.time + 0.25f;
    }

    private void MoveDown() {
        if (highlightedIdx >= classEntryDisplayers.Count - 1) return;
        highlightedDisplayer.UnHighlight();
        highlightedIdx++;
        highlightedDisplayer = classEntryDisplayers[highlightedIdx];
        highlightedDisplayer.Highlight();
        timeOfNextTransition = Time.time + 0.25f;
    }

}
