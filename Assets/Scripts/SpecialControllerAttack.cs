﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class SpecialControllerAttack : SpecialController {

    public AttackController AttackController;

    // Release Special - Called on 1 Frame
    // This is when the special button is released.
    public override void ActivateSpecial(SpecialData data) {
        AttackData attackData = new AttackData() {
            OwnerEntityData = data.OwnerEntityData,
            AimVector2D = data.AimVector2D
        };
        AttackController.ReleaseAttack(attackData);
    }
    
}
