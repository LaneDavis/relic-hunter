﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

public class FollowTransform : MonoBehaviour {

    public Vector3 Offset;

    // Movement toward Target
    public AnimationCurve ApproachSpeedByDistance;
    public AnimationCurve ZoomMultiplierByPlayerSpread;

    public Vector3 followedPosition;
    private float playerSpread;

    // Movement Prediction
    public bool PredictsMovement;
    [ShowIf("PredictsMovement")] public RollingBiasedAverage PredictionAverage;
    [ShowIf("PredictsMovement")] public float PredictionSeconds;
    private Vector3 lastTransformPosition;
    private Vector3 predictedOffset;
    
    // Damping
    [HideIf("PredictsMovement")] public float DampingAccel;
    private Vector3 targetPos;
    private Vector3 dampVelocity;
    public float YChaseMultiplier = 0.33f;
    
    [ShowIf("PredictsMovement")] public float DampAccelExtending;
    [ShowIf("PredictsMovement")] public float DampAccelRetracting;
    [ShowIf("PredictsMovement")] public float DampAccelAccel = 6f;
    private float curAccel;
    private float curAccelVelocity;

    private void Start() {
        
        lastTransformPosition = followedPosition;
        UpdateTargetPosition();
        
        targetPos = followedPosition + Offset;
        if (PredictsMovement) {
            curAccel = DampAccelRetracting;
        }
        
    }
    
    private void Update() {

        UpdateTargetPosition();
        
        // Predict future movement
        if (PredictsMovement) {
            PredictionAverage.UpdateValue((followedPosition - lastTransformPosition) / Time.deltaTime);
            Vector3 newPredictedOffset = PredictionAverage.GetVector3Value() * PredictionSeconds;
            if (newPredictedOffset.magnitude > predictedOffset.magnitude + 0.01f) {
                curAccel = Mathf.SmoothDamp(curAccel, DampAccelExtending, ref curAccelVelocity, 1f / DampAccelAccel);
            } else if (newPredictedOffset.magnitude < predictedOffset.magnitude - 0.01f) {
                curAccel = Mathf.SmoothDamp(curAccel, DampAccelRetracting, ref curAccelVelocity, 1f / DampAccelAccel);
            }
            predictedOffset = PredictionAverage.GetVector3Value() * PredictionSeconds;
            predictedOffset.y = 0f;
            lastTransformPosition = followedPosition;
        }
        
        // Moved toward desired position
        Vector3 TargetPosition = followedPosition + Offset * ZoomMultiplierByPlayerSpread.Evaluate(playerSpread) + predictedOffset;
        TargetPosition.y = targetPos.y * (1f - YChaseMultiplier) + TargetPosition.y * YChaseMultiplier;
        targetPos = Vector3.SmoothDamp(targetPos, TargetPosition, ref dampVelocity, 1f / (PredictsMovement ? curAccel : DampingAccel));
        Vector3 Diff = targetPos - transform.position;
        transform.Translate(Diff.normalized * ApproachSpeedByDistance.Evaluate(Diff.magnitude) * Time.deltaTime, Space.World);

    }

    private void UpdateTargetPosition () {

        List<GameObject> TrackedPlayers = PlayerManager.Instance.PlayerObjects();
        if (TrackedPlayers.Count < 1) {
            followedPosition = lastTransformPosition;
            playerSpread = 0f;
            return;
        }
        Vector3 sumPosition = Vector3.zero;
        TrackedPlayers.ForEach(e => sumPosition += e.transform.position);
        followedPosition = sumPosition / TrackedPlayers.Count;
        lastTransformPosition = followedPosition;
        
        // Calculate player spread (furthest distance between players)
        float largestSpreadSoFar = 0f;
        foreach (GameObject g in TrackedPlayers) {
            foreach (GameObject other in TrackedPlayers) {
                float distance = Vector3.Distance(g.transform.position, other.transform.position);
                if (distance > largestSpreadSoFar) largestSpreadSoFar = distance;
            }
        }

        playerSpread = largestSpreadSoFar;

    }

}
