﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HealthBar : MonoBehaviour {

    public Gradient Gradient;
    private Vector2 baseSizeDelta;

    public Image BarImage;
    public RectTransform BarRT;

    private void Awake() {
        baseSizeDelta = BarRT.sizeDelta;
    }

    public void SetHP(float pct) {
        BarRT.sizeDelta = new Vector2(baseSizeDelta.x * pct, baseSizeDelta.y);
        BarImage.color = Gradient.Evaluate(pct);
    }

}
