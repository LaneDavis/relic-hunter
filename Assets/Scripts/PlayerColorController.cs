﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.UI;

public class PlayerColorController : MonoBehaviour {
    
    public List<Image> ColoredImages = new List<Image>();
    private Color fullColor;

    [Range(0f, 1f)] public float TintPercent = 0.15f;
    public List<SpriteRenderer> TintedSprites = new List<SpriteRenderer>();
    private Color tintColor;

    public List<FxColor> WatchedColorFXs;
    
    public void SetColors(Color rootColor) {
        fullColor = rootColor;
        tintColor = ColorUtilities.BlendColors(new List<ColorWeight>() {new ColorWeight(rootColor, TintPercent), new ColorWeight(Color.white, 1f - TintPercent)});
        
        // We want to ensure that the FxColor scripts use the tint color as a base color instead of white.
        foreach (FxColor fxc in WatchedColorFXs) {
            fxc.OnSetUp -= RefreshApplierBaseColor;    // Remove old instances of this method.
            fxc.OnSetUp += RefreshApplierBaseColor;    // Add new instance of RefreshApplierBaseColor method.
        }

        foreach (Image img in ColoredImages) {
            img.color = fullColor;
        }

        foreach (SpriteRenderer spr in TintedSprites) {
            spr.color = tintColor;
        }
    }

    private void RefreshApplierBaseColor(FxColor fxc) {
        if (fxc.applier != null) {
            fxc.applier.SetBaseColor(tintColor);
        }
    }

}
