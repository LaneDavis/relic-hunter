﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameController : EntityAttachment {

    public float Duration = 10f;
    public float DamagePerTick = 5f;

    public List<ParticleSystem> Particles = new List<ParticleSystem>();

    public float FadeTime = 1f;
    private float fadeTimer;
    private float FadeProgress => fadeTimer / FadeTime;
    public AnimationCurve ParticleSpeedCurve;
    private bool isFading;
    
    public override void OnAttached(EntityController newEntityController) {
        base.OnAttached(newEntityController);
        entityController.OnDOTTick += FlameTick;
        entityController.OnDeath += DetachAndFade;
    }

    public override bool CanMakeNewAttachmentCheck(EntityAttachment newAttachment) {
        if (newAttachment != null && newAttachment is FlameController && StartTime <= newAttachment.StartTime) {
            return false;
        }

        return true;
    }

    private void Update() {
        if (Time.time - StartTime > Duration) {
            Fade();
        }

        if (isFading) {
            fadeTimer += Time.deltaTime;
            foreach (ParticleSystem ps in Particles) {
                var mainModule = ps.main;
                mainModule.simulationSpeed = ParticleSpeedCurve.Evaluate(FadeProgress);
            }

            if (FadeProgress >= 1f) {
                Destroy(gameObject);
            }
        }
    }

    private void FlameTick(EntityController controller) {
        if (isFading) return;
        controller.TakeDamage(DamagePerTick);
    }

    private void Fade() {
        if (isFading) return;
        isFading = true;
        Particles.ForEach(e => e.Stop());
    }

    public void DetachAndFade(EntityController controller) {
        transform.parent = null;
        Fade();
    }
    
    public override void OnDestroy() {
        base.OnDestroy();
        if (entityController != null) {
            entityController.OnDOTTick -= FlameTick;
        }
    }
    
}
