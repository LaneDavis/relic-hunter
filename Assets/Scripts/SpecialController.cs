﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SpecialController : MonoBehaviour {
    
    // Cooldown — Time between special uses.
    [Range(0f, 60f)] public float Cooldown = 2f;
    private float timeOfNextUse;
    
    // Cooldown Animations
    private Animator animator;
    private bool IsOnCooldown => Time.time < timeOfNextUse;
    private bool wasOnCooldown;
    private const string ExhaustAnimation = "Special Indicator Exhaust";
    private const string RechargeAnimation = "Special Indicator Recharge";

    public void Init(Animator specialAnimator) {
        animator = specialAnimator;
    }
    
    // Start Special — Called on 1 Frame
    // This is when the special button is pressed.
    public virtual void StartSpecial(SpecialData data) {
        
    }

    // Hold Special — Called Once per Frame
    // This is whenever the special button is held.
    public virtual void HoldSpecial(SpecialData data) {
        
    }

    // Release Special — Called on 1 Frame
    // This is when the special button is released.
    public virtual void ReleaseSpecial(SpecialData data) {
        if (Time.time >= timeOfNextUse) {
            ActivateSpecial(data);
        }
    }

    // Activate Special — Executes the effect of the special.
    public virtual void ActivateSpecial(SpecialData data) {
        timeOfNextUse = Time.time + Cooldown;
    }

    protected virtual void Update() {
        if (IsOnCooldown && !wasOnCooldown) {
            animator.Play(ExhaustAnimation);
        }
        else if (wasOnCooldown && !IsOnCooldown) {
            animator.Play(RechargeAnimation);
        }

        wasOnCooldown = IsOnCooldown;
    }
    
}

public class SpecialData {

    public PlayerData OwnerPlayerData;
    public EntityData OwnerEntityData;
    public Vector2 AimVector2D;

}