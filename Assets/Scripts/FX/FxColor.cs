﻿using System;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class FxColor : FxVariableValue {

	//FACTOR ATTRIBUTES
	public Color Color = Color.black;

	//ANATOMY
	public FxApplierColor applier { get; private set; }
	
	public delegate void SetUpDelegate(FxColor thisScript);
	public event SetUpDelegate OnSetUp;

	public override void SetUp (GameObject newAnchor = null) {

		base.SetUp(newAnchor);
		if (newAnchor == null) return;
		Anchor = newAnchor;
 
		if (newAnchor.GetComponent<FxApplierColor>() != null) {
			applier = newAnchor.GetComponent<FxApplierColor>();
		} else {
			applier = newAnchor.AddComponent<FxApplierColor>();
			applier.SetUp(newAnchor);
		}

		OnSetUp?.Invoke(this);

	}

	private void Update () {

		//If toggled on, set the color value based on a gradient.
		if (ToggleState) {
			Debug.LogWarning("Toggled application is not implemented for FxColor");
		}

	}

	public override void Trigger (GameObject newAnchor = null, float power = 1f) {
		base.Trigger(newAnchor);
		applier.AddColorFactor(SfxId, MyValueFactor(power), Color);
	}
	
	public override void Toggle (bool toggleState, GameObject newAnchor = null, float power = 1f) {
		base.Toggle(toggleState, newAnchor);
		if (toggleState)
			applier.AddColorFactor(SfxId, MyValueFactor(power), Color);
		else
			applier.EndFactor(SfxId);
	}
	
}

#if UNITY_EDITOR
[CanEditMultipleObjects]
[CustomEditor(typeof(FxColor))]
public class FxColorEditor : Editor {
	override public void OnInspectorGUI() {
        
		var myScript = target as FxColor;
        
		List<string> excludedProperties = new List<string>();
		serializedObject.Update();

		if(myScript.Style != ValueFactor.FactorStyle.Timed){
			excludedProperties.Add("Timed_Duration");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Decay){
			excludedProperties.Add("Decay_DecayRate");
			excludedProperties.Add("Decay_MaxDuration");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Curve){
			excludedProperties.Add("Curve_Curve");
		}
		else {
			excludedProperties.Add("BaseValue");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Toggle){
			excludedProperties.Add("Toggle_Easing");
		}

		if (myScript.Style != ValueFactor.FactorStyle.Toggle || !myScript.Toggle_Easing) {
			excludedProperties.Add("Toggle_EaseIn");
			excludedProperties.Add("Toggle_EaseOut");
		}

		DrawPropertiesExcluding(serializedObject, excludedProperties.ToArray());
		serializedObject.ApplyModifiedProperties();

	}
}
#endif