﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class FxRotate : FxVariableValue {

	//ANATOMY
	private FxApplierRotate applier;

	public override void SetUp (GameObject newAnchor = null) {

		base.SetUp(newAnchor);
		if (newAnchor == null) return;
		Anchor = newAnchor;

		if (newAnchor.GetComponent<FxApplierRotate>() != null) {
			applier = newAnchor.GetComponent<FxApplierRotate>();
		} else {
			applier = newAnchor.AddComponent<FxApplierRotate>();
			applier.SetUp(newAnchor);
		}

	}

	public override void Trigger (GameObject newAnchor = null, float power = 1f) {
		base.Trigger(newAnchor);
		applier.SetRotateFactor(SfxId, MyValueFactor(power));
	}

	public override void Toggle (bool toggleState, GameObject newAnchor = null, float power = 1f) {
		base.Toggle(toggleState, newAnchor);
		if (toggleState)
			applier.SetRotateFactor(SfxId, MyValueFactor(power));
		else
			applier.EndFactor(SfxId);
	}

}

#if UNITY_EDITOR
[CanEditMultipleObjects]
[CustomEditor(typeof(FxRotate))]
public class FxRotateEditor : Editor {
	override public void OnInspectorGUI() {
        
		var myScript = target as FxRotate;
        
		List<string> excludedProperties = new List<string>();
		serializedObject.Update();

		if(myScript.Style != ValueFactor.FactorStyle.Timed){
			excludedProperties.Add("Timed_Duration");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Decay){
			excludedProperties.Add("Decay_DecayRate");
			excludedProperties.Add("Decay_MaxDuration");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Curve){
			excludedProperties.Add("Curve_Curve");
		}
		else {
			excludedProperties.Add("BaseValue");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Toggle){
			excludedProperties.Add("Toggle_Easing");
		}

		if (myScript.Style != ValueFactor.FactorStyle.Toggle || !myScript.Toggle_Easing) {
			excludedProperties.Add("Toggle_EaseIn");
			excludedProperties.Add("Toggle_EaseOut");
		}

		DrawPropertiesExcluding(serializedObject, excludedProperties.ToArray());
		serializedObject.ApplyModifiedProperties();

	}
}
#endif
