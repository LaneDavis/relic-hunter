﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class FxTranslate : FxVariableValue {

	//FACTOR ATTRIBUTES
	public Vector2 DirectionVector = Vector2.up;

	//ANATOMY
	private FxApplierTranslate applier;

	public override void SetUp (GameObject newAnchor = null) {

		base.SetUp(newAnchor);
		if (newAnchor == null) return;
		Anchor = newAnchor;

		if (newAnchor.GetComponent<FxApplierTranslate>() != null) {
			applier = newAnchor.GetComponent<FxApplierTranslate>();
		} else {
			applier = newAnchor.AddComponent<FxApplierTranslate>();
			applier.SetUp(newAnchor);
		}

	}

	public override void Trigger (GameObject newAnchor = null, float power = 1f) {
		base.Trigger(newAnchor);
		applier.SetTranslateFactor(SfxId, DirectionVector, MyValueFactor(power), false);
	}

	public override void Toggle (bool toggleState, GameObject newAnchor = null, float power = 1f) {
		base.Toggle(toggleState, newAnchor);
		if (toggleState) 
			applier.SetTranslateFactor(SfxId, DirectionVector, MyValueFactor(power, SfxId), false);
		else 
			applier.EndFactor(SfxId);
	}

}

#if UNITY_EDITOR
[CanEditMultipleObjects]
[CustomEditor(typeof(FxTranslate))]
public class FxTranslateEditor : Editor {
	override public void OnInspectorGUI() {
        
		var myScript = target as FxTranslate;
        
		List<string> excludedProperties = new List<string>();
		serializedObject.Update();

		if(myScript.Style != ValueFactor.FactorStyle.Timed){
			excludedProperties.Add("Timed_Duration");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Decay){
			excludedProperties.Add("Decay_DecayRate");
			excludedProperties.Add("Decay_MaxDuration");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Curve){
			excludedProperties.Add("Curve_Curve");
		}
		else {
			excludedProperties.Add("BaseValue");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Toggle){
		}

		DrawPropertiesExcluding(serializedObject, excludedProperties.ToArray());
		serializedObject.ApplyModifiedProperties();

	}
}
#endif

