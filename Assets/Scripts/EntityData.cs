﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityData {

    public EntityBaseStats BaseStats;
    public float MaxHP;
    public float CurHP;
    public GameObject EntityObject;
    public List<Collider> EntityColliders;
    
    // Buff Status
    public BuffController BuffController;
    public float Buff_SpeedMultiplier => BuffController.MoveSpeedBuffs.Value;
    
    public EntityData(EntityBaseStats baseStats, List<Collider> colliders, BuffController buffController) {
        BaseStats = baseStats;
        MaxHP = baseStats.MaxHP;
        CurHP = MaxHP;
        EntityColliders = colliders;
        BuffController = buffController;
    }

}
