﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Sirenix.OdinInspector;
using UnityEngine;

public class ProjectileController : MonoBehaviour {

    public float Damage;
    [HideInInspector] public EntityData OwnerEntityData;
    [HideInInspector] public PlayerData OwnerPlayerData;
    public List<Collider> colliders = new List<Collider>();
    public const float NoCollisionTime = 0.0333f;
    private float noCollisionTimer;
    public TrailRenderer TrailRenderer;

    public float LateralSpeed;
    public float VerticalSpeed;
    public bool normalizeInputVector;
    private float elapsedTime;

    public bool isPenetrating;
    // Scaling
    public bool ScalesOverTime;
    private Vector3 baseScale = Vector3.one;
    [ShowIf("ScalesOverTime")] public AnimationCurve SizeOverLifetime = new AnimationCurve(new Keyframe[] {new Keyframe(1f, 1f), new Keyframe(1f, 1f)});

    [Header("Bullet Death")]
    public bool TimedLife;
    [ShowIf("TimedLife")] public float TimedLifeDuration;
    public BulletDeathEffects DeathEffects;

    [Header("Attachments")]
    public List<GameObject> AttachmentsToTarget;
    public List<Buff> BuffsApplied;
    
    // Kinematic Movement
    private bool isKinematic = false;
    private Vector3 kinematicVector;

    public void Initialize(AttackData data) {

        if (ScalesOverTime) {
            baseScale = transform.localScale;
            transform.localScale = baseScale * SizeOverLifetime.Evaluate(0f);
        }
        
        OwnerEntityData = data.OwnerEntityData;
        OwnerPlayerData = data.OwnerPlayerData;

        if (normalizeInputVector) {
            data.AimVector2D = data.AimVector2D.normalized;
        }
        
        Rigidbody newRigidbody = GetComponent<Rigidbody>();
        if (newRigidbody != null) {
            float mag = data.AimVector2D.magnitude;
            newRigidbody.AddForce(new Vector3(data.AimVector2D.x * LateralSpeed * 100f, mag * VerticalSpeed * 100f, data.AimVector2D.y * LateralSpeed * 100f));
        }
        isKinematic = newRigidbody.isKinematic;
        if (isKinematic) {
            kinematicVector = new Vector3 (data.AimVector2D.x, 0f, data.AimVector2D.y);
        }
        
        // Set Trail Color
        if (OwnerPlayerData != null && TrailRenderer != null) {
            TrailRenderer.startColor = OwnerPlayerData.Color;
            TrailRenderer.endColor = OwnerPlayerData.Color;
        }

        for (int i = 0; i < colliders.Count; i++) {
            for (int j = 0; j < data.OwnerEntityData.EntityColliders.Count; j++) {
                Physics.IgnoreCollision(colliders[i], data.OwnerEntityData.EntityColliders[j]);
            }
        }
        
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.layer == 13 /*Entity Layer*/) {
            EntityController controller = other.gameObject.GetComponent<EntityController>();

            if (controller != null) {

                // Don't hit the bullet's owner.
                if (controller.EntityData != null && OwnerEntityData != null && controller.EntityData == OwnerEntityData) {
                    return;
                }

                controller.TakeDamage(Damage);
                foreach (GameObject attachment in AttachmentsToTarget) {
                    controller.AddAttachment(attachment);
                }

                foreach (Buff buff in BuffsApplied) {
                    controller.EntityData.BuffController.AddBuff(buff);
                }
            }
        }
    }


    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.layer == 13 /*Entity Layer*/) {
            EntityController controller = collision.gameObject.GetComponent<EntityController>();
            
            if (controller != null) {
                
                // Don't hit the bullet's owner.
                if (controller.EntityData != null && OwnerEntityData != null && controller.EntityData == OwnerEntityData) {
                    return;
                }
                
                controller.TakeDamage(Damage);
                foreach (GameObject attachment in AttachmentsToTarget) {
                    controller.AddAttachment(attachment);
                }

                foreach (Buff buff in BuffsApplied) {
                    controller.EntityData.BuffController.AddBuff(buff);
                }
            }
        }

        if (!isPenetrating || collision.gameObject.layer == 9 /*Terrain Layer*/) {
            KillProjectile();
        }
    }

    private void Update() {
        noCollisionTimer += Time.deltaTime;
        elapsedTime += Time.deltaTime;
        if (noCollisionTimer > NoCollisionTime) {
            colliders.ForEach(e => e.enabled = true);
        }

        if (isKinematic) {
            transform.Translate(kinematicVector * LateralSpeed * Time.deltaTime);
        }

        // Scale Over Time
        if (ScalesOverTime) {
            transform.localScale = baseScale * SizeOverLifetime.Evaluate(elapsedTime);
        }
        
        if (TimedLife && elapsedTime > TimedLifeDuration) {
            KillProjectile();
        }
        
        if (transform.position.y < KillFloor.KillFloorHeight) {
            KillProjectile();
        }
    }

    public void KillProjectile() {
        
        // Detach object that carries persistent elements like particles or sound effects.
        if (DeathEffects != null) DeathEffects.DetachAndPlayDeathEffects();
        
        // Destroy the projectile object.
        Destroy(gameObject);
    }
    
}
