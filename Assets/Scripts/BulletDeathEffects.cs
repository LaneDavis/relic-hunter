﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDeathEffects : MonoBehaviour {

    public float LifeTime = 2f;
    public List<ParticleSystem> ParticlesPlayed;

    public void DetachAndPlayDeathEffects() {
        transform.parent = null;
        foreach (ParticleSystem ps in ParticlesPlayed) {
            ps.Play();
        }
        Invoke(nameof(KillThis), LifeTime);
    }

    private void KillThis() {
        Destroy(gameObject);
    }

}
