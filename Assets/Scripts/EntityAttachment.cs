﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityAttachment : MonoBehaviour {

    protected EntityController entityController;
    [HideInInspector] public float StartTime;
    
    public virtual void OnAttached(EntityController newEntityController) {
        StartTime = Time.time;
        entityController = newEntityController;
    }

    public virtual bool CanMakeNewAttachmentCheck(EntityAttachment newAttachment) {
        return true;
    }

    public virtual void OnDestroy() {
        entityController.UnsubscribeAttachment(this);
    }
    
}
