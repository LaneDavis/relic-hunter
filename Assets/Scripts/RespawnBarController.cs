﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RespawnBarController : MonoBehaviour {
    
    public static List<RespawnBarController> RespawnBarControllers = new List<RespawnBarController> ();
    private PlayerData playerData;

    public FxController NoticePulseFX;

    private bool playerWasDead;

    [Header("Bar")]
    public Image PlayerColorCircle;
    public Image BarImage;
    public RectTransform BarRT;
    public RectTransform BarWidthIndicatorRT;
    
    private Vector2 baseSizeDelta;
    private float BaseWidth => BarWidthIndicatorRT.rect.width;

    [Header("Flash")]
    public AnimationCurve FlashRateByProgress;
    public float FlashBlendStrength = 0.5f;
    private float flashTimer;
    private bool flashOn;
    
    [Header("Masker")]
    public RectTransform MaskerRT;
    public float MaskerExpandTime;
    private float maskerVelocity;
    private float maskerCurMag;

    private bool initialized;
    private float playerDeathTime;
    
    private void Awake() {
        baseSizeDelta = BarRT.sizeDelta;
        RespawnBarControllers.Add(this);
    }

    public void Init(PlayerData newPlayerData) {
        if (initialized) return;
        initialized = true;
        playerData = newPlayerData;
        BarImage.color = newPlayerData.Color;
        PlayerColorCircle.color = newPlayerData.Color;
        newPlayerData.RespawnBarController = this;
    }

    private void Update() {

        if (!initialized) return;

        bool playerIsDead = playerData.IsDead;

        if (playerIsDead != playerWasDead) {
            NoticePulseFX.Trigger();
        }
        
        if (playerIsDead && !playerWasDead) {
            playerDeathTime = Time.time;
        }

        float progress = (Time.time - playerDeathTime) / (playerData.RespawnTime - playerDeathTime);
        if (playerIsDead) {
            float timeSpentDead = Time.time - playerDeathTime;
            float totalDeathDuration = playerData.RespawnTime - playerDeathTime;
            SetBarProgress(1f - (Time.time - playerDeathTime) / (playerData.RespawnTime - playerDeathTime));
            
            // Update Bar Flash
            if (progress < 0.5f) {
                flashOn = false;
            }
            else {
                flashTimer += FlashRateByProgress.Evaluate(progress) * Time.deltaTime;
                if (flashTimer > 1f) {
                    flashTimer = 0f;
                    flashOn = !flashOn;
                }
            }

            if (!flashOn) BarImage.color = playerData.Color;
            else BarImage.color = ColorUtilities.BlendColors(new List<ColorWeight>(){new ColorWeight(playerData.Color, 1f - FlashBlendStrength), new ColorWeight(Color.white, FlashBlendStrength)});

        }
        
        
        // Resize Masker
        maskerCurMag = Mathf.SmoothDamp(maskerCurMag, playerIsDead ? 1f : 0.001f, ref maskerVelocity, MaskerExpandTime);
        MaskerRT.localScale = new Vector3(maskerCurMag, maskerCurMag, maskerCurMag);

        playerWasDead = playerIsDead;

    }

    private void SetBarProgress(float pct) {
        BarRT.sizeDelta = new Vector2(baseSizeDelta.x - BaseWidth * pct, baseSizeDelta.y);
    }

}
