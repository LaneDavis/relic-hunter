﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveManager : MonoBehaviour {

    public static ObjectiveManager Instance;
    private float totalProgress;

    private void Awake() {
        Instance = this;
    }

    public void ReportProgress(float progressAmount) {
        totalProgress = Mathf.Clamp01(totalProgress += progressAmount);
        if (ObjectiveDisplayer.Instance != null) {
            ObjectiveDisplayer.Instance.SetProgress(totalProgress);
        }

        if (totalProgress >= 1f) {
            PlayerManager.Instance.ReportVictory();
        }
    }
    
}
