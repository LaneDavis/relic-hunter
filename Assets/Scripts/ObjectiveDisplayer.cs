﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveDisplayer : MonoBehaviour {

    public static ObjectiveDisplayer Instance;
    public RectTransform BarRT;
    public Image BarImage;
    public TextMeshProUGUI ObjectiveText;
    public TextMeshProUGUI ProgressText;

    public Color ProgressColor;
    public Color CompleteColor;

    public Animator FlowbarAnimator;
    public AnimationCurve FlowbarSpeedByTimeSinceLastProgress;
    private float lastProgressTime;

    private void Awake() {
        Instance = this;
    }

    public void SetObjectiveText(string objectiveText) {
        ObjectiveText.text = objectiveText;
    }

    public void SetProgress(float pctProgress) {
        pctProgress = Mathf.Clamp01(pctProgress);
        int intProgress = (int) (pctProgress * 100);
        ProgressText.text = intProgress + "%";
        BarRT.transform.localScale = new Vector3(Mathf.Clamp(pctProgress, 0.0001f, 1f), BarRT.transform.localScale.y);
        BarImage.color = pctProgress >= 1f ? CompleteColor : ProgressColor;
        lastProgressTime = Time.time;
    }

    private void Update() {
        FlowbarAnimator.speed = FlowbarSpeedByTimeSinceLastProgress.Evaluate(Time.time - lastProgressTime);
    }

}
