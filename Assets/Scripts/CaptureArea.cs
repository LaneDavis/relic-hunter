﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CaptureArea : MonoBehaviour {

    public float CaptureSpeed = 0.1f;
    public float BoundsBonusArea = 0.5f;
    
    // BOUNDS
    private float xMin = Mathf.Infinity;
    private float xMax = Mathf.NegativeInfinity;
    private float zMin = Mathf.Infinity;
    private float zMax = Mathf.NegativeInfinity;
    
    private void Start() {
        List<Transform> children = GetComponentsInChildren<Transform>().ToList();
        foreach (Transform t in children) {
            Vector3 tPos = t.position;
            if (tPos.x < xMin) xMin = tPos.x;
            if (tPos.x > xMax) xMax = tPos.x;
            if (tPos.z < zMin) zMin = tPos.z;
            if (tPos.z > zMax) zMax = tPos.z;
        }
    }
    
    private void Update() {
        if (PlayerInBounds()) {
            ObjectiveManager.Instance.ReportProgress(CaptureSpeed * Time.deltaTime);
        }
        else {
            ObjectiveManager.Instance.ReportProgress(-CaptureSpeed * Time.deltaTime);
        }
    }

    private bool PlayerInBounds() {
        List<GameObject> activePlayers = PlayerManager.Instance.PlayerObjects();
        foreach (GameObject player in activePlayers) {
            Vector3 playerPos = player.transform.position;
            if (playerPos.x < xMin - BoundsBonusArea) continue;
            if (playerPos.x > xMax + BoundsBonusArea) continue;
            if (playerPos.z < zMin - BoundsBonusArea) continue;
            if (playerPos.z > zMax + BoundsBonusArea) continue;
            return true;
        }

        return false;
    }
    
}
