﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class SpecialControllerBlock : SpecialController {

    public GameObject BlockPrefab;

    // Release Special - Called on 1 Frame
    // This is when the special button is released.
    public override void ActivateSpecial(SpecialData data) {
        base.ActivateSpecial(data);
        Vector3 positionVector3 = transform.position + new Vector3(data.AimVector2D.x, 0f, data.AimVector2D.y);
        positionVector3.x = Mathf.Round(positionVector3.x  * 2f) / 2f;
        positionVector3.y = Mathf.Round(positionVector3.y  * 2f) / 2f;
        positionVector3.z = Mathf.Round(positionVector3.z  * 2f) / 2f;
        Instantiate(BlockPrefab, positionVector3, Quaternion.identity, null); 
    }
    
}
