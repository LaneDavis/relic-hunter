﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantRotation : MonoBehaviour {

    public Vector3 EulerAngles;

    private void Update() {
        transform.Rotate(EulerAngles * Time.deltaTime);
    }
    
}
