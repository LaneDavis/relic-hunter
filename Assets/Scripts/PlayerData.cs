﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class PlayerData {

    // Anatomy
    public GameObject PlayerObject;
    public EntityController Controller;
    public PlayerInput PlayerInput;
    public PlayerColorController ColorController;
    
    // Permanent Data
    public EntityData EntityData;
    public EntityBaseStats BaseStats;
    public Color Color;
    public int PlayerNumber;
    public Player RewiredPlayer;

    // Temporary Data
    public float RumbleAmount;
    
    // Class Selection
    public bool IsClassSelecting;
    public ClassSelectionController ClassSelectionController;
    
    // Death
    public int DeathCount;
    public bool IsDead;
    public float RespawnTime;
    public RespawnBarController RespawnBarController;

    public void AddRumble(float newAmount) {
        RumbleAmount = Mathf.Max(RumbleAmount, Mathf.Clamp01(newAmount));
    }

}
