﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyInput {

    void WarpToMesh(Vector3 point);

}
