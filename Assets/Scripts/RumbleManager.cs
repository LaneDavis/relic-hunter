﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RumbleManager : MonoBehaviour {

    private List<PlayerData> playerDatas = new List<PlayerData>();
    public float RumbleDecayRate = 8f;

    private void Update() {
        foreach (PlayerData pd in playerDatas) {
            pd.RewiredPlayer.SetVibration(0, pd.RumbleAmount);
            pd.RewiredPlayer.SetVibration(1, pd.RumbleAmount);
            pd.RumbleAmount = MathUtilities.Decay(pd.RumbleAmount, RumbleDecayRate, Time.deltaTime);
        }
    }

    public void AddPlayerData(PlayerData newPlayerData) {
        playerDatas.Add(newPlayerData);
    }
    
}
