﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[Serializable]
public class RollingBiasedAverage {

    public enum Style { Time, Iteration };
    public Style RollStyle;
    public bool RollStyleIsTime => RollStyle == Style.Time;
    public bool RollStyleIsIteration => RollStyle == Style.Iteration;
    
    [ShowIf("RollStyleIsTime")] public float WindowDuration;
    [ShowIf("RollStyleIsIteration")] public float WindowCount;

    private List<RollingSummable> summables = new List<RollingSummable>();
    private Vector3 lastValue = Vector3.zero;
    
    public AnimationCurve WeightCurve = new AnimationCurve(
        new Keyframe(0f, 1f, -3f, -3f, .4f, .4f),
        new Keyframe(1f, 0f, 0f, 0f, .7f, .7f));

    // Update Value with Floats
    public void UpdateValue (float value, float value2 = 0f, float value3 = 0f) {
        summables.Insert(0, new RollingSummable(new Vector3(value, value2, value3)));
        lastValue = new Vector3(value, value2, value3);
    }
    
    // Update Value with Vectors
    public void UpdateValue(Vector3 valueVector) {
        summables.Insert(0, new RollingSummable(valueVector));
        lastValue = valueVector;
    }
    
    // Get Float
    public float GetFloatValue () {
        return GetValue().x;
    }
    
    // Get Vector2
    public Vector2 GetVector2Value() {
        return GetValue();
    }
    
    // Get Vector3
    public Vector3 GetVector3Value() {
        return GetValue();
    }

    private Vector3 GetValue() {
        PruneOldSummables();
        if (summables.Count < 1) return lastValue;
        Vector3 totalVal = Vector3.zero;
        float totalWeight = 0f;
        for (int i = 0; i < summables.Count; i++) {
            float weight = WeightOfSummable(i);
            totalVal += summables[i].Value * weight;
            totalWeight += weight;
        }
        return totalVal / totalWeight;
    }

    private float WeightOfSummable(int idx) {
        if (RollStyle == Style.Iteration) {
            float elapsed = idx / WindowCount;
            return WeightCurve.Evaluate(elapsed);
        }
        else if (RollStyle == Style.Time) {
            float elapsed = Time.time - summables[idx].TimeOfAddition;
            return WeightCurve.Evaluate(elapsed);
        }
        return 0f;
    }
    
    private void PruneOldSummables() {
        for (int i = summables.Count - 1; i >= 0; i--) {
            if (RollStyle == Style.Iteration) {
                if (i >= WindowCount) summables.RemoveAt(i);
                else return;
            }
            else if (RollStyle == Style.Time) {
                if (summables[i].TimeOfAddition < Time.time - WindowDuration) summables.RemoveAt(i);
                else return;
            }
        }
    }
    
}

public class RollingSummable {

    public Vector3 Value;
    public float TimeOfAddition;

    public RollingSummable(Vector3 value) {
        Value = value;
        TimeOfAddition = Time.time;
    }

}