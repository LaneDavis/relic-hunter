﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtensions {

    public static void SetLayerOfHierarchy (this GameObject g, int layerNum) {
        g.layer = layerNum;
        foreach (Transform t in g.transform) {
            t.gameObject.layer = layerNum;
        }
    }

    public static T GetComponentInAncestry<T>(this GameObject g) {
        if (g.transform.parent == null) return default(T);
        Transform curAncestor = g.transform.parent;
        while (true) {
            T ret = curAncestor.GetComponent<T>();
            if (ret != null) return ret;
            if (curAncestor.parent == null) return default(T);
            curAncestor = curAncestor.parent;
        }
    }
    
}
