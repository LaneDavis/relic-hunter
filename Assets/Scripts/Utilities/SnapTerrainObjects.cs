﻿using Sirenix.OdinInspector;
using UnityEngine;

public class SnapTerrainObjects : MonoBehaviour {

    [Button("Snap Self")]
    public void SnapSelf() {
        Vector3 oldPosition = transform.position;
        transform.position = new Vector3(Mathf.Round(oldPosition.x * 2f) * 0.5f, Mathf.Round(oldPosition.y * 2f) * 0.5f, Mathf.Round(oldPosition.z * 2f) * 0.5f);
    }

    [Button("Snap Terrain")]
    public void SnapTerrain() {
        foreach (Collider c in GetComponentsInChildren<Collider>()) {
            GameObject go = c.gameObject;
            if (go.layer == 9 || go.layer == 11) {
                Vector3 oldPosition = go.transform.position;
                go.transform.position = new Vector3(Mathf.Round(oldPosition.x * 2f) * 0.5f, Mathf.Round(oldPosition.y * 2f) * 0.5f, Mathf.Round(oldPosition.z * 2f) * 0.5f);
            }
        }
    }
    
}
