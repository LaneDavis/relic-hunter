﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Runtime.CompilerServices;
using System.Text;


/// <summary>A simplified version of WeakHashMap that uses identity comparison (==) for keys. It is simplified in that it does not implement almost any part of the Map interface.
/// Only get, put, and remove exist. This class does not implement the map interface because it is so specific in its use at this point that it's not worth it.</summary>
public class WeakIdentityDictionary<TKey, TValue> 
	where TKey : class
	where TValue : class
{
	private const int INITIAL_BUCKET_COUNT = 16;
	
	/// <summary>An array containing linked lists of entries weak entries.</summary>
	private WeakEntry[] buckets;
	
	/// <summary>Used to find concurrent modifications</summary>
	private int modCount;
	
	/// <summary>Last known number of live items in this map</summary>
	private int size;
	
	/// <summary>Once size reaches one of these values, increase or decrease capacity</summary>
	private int maxThreshold, minThreshold;
	
	/// <summary>Entries into the map. Acts as a Key/Value pair, a weak key reference, and a linked list entry. This class handles all storage and removal operations (but does not adjust modCount).</summary>
	private class WeakEntry : WeakReference
	{
		public WeakEntry prev;
		public WeakEntry next;
		public int bucketIndex;
		public TValue value;
		private WeakIdentityDictionary<TKey, TValue> parent;
		
		public WeakEntry(TKey key, TValue value, int bucketIndex, WeakIdentityDictionary<TKey, TValue> parent) : base(key) {
			this.parent = parent;
			parent.size++;
			this.value = value;
			setBucketIndex(bucketIndex);
		}
		
		/// <summary>Add this item to the specified bucket. Note that this does not clean up from any previous buckets. This is because this is only called when
		/// first created (and there is no previous bucket) or when rehashing (and all buckets are being reset anyway).</summary>
		public void setBucketIndex(int index) {
			bucketIndex = index;
			if (parent.buckets[bucketIndex] != null)	//Link the first item in the bucket to this
				parent.buckets[bucketIndex].prev = this;
			next = parent.buckets[bucketIndex];			//Link this to the first item in the bucket.
			parent.buckets[bucketIndex] = this;			//Set this as the new first item in the bucket.
			prev = null;								//We're at the front, meaning we don't have anything before us.
		}
		
		public override string ToString()
		{
			object key = Target;
			if (key == null)
				return "(gone)";
			else
				return key + "=" + value;
		}
		
		public bool ToString(StringBuilder sb) {
			object key = Target;
			if (key != null) {
				sb.Append(key);
				sb.Append("=");
				sb.Append(value);
				return true;
			}
			return false;
		}
		
		/// <summary>Removes this item from its bucket (if not already removed). Cleans up after itself by linking its previous and next items together,
		/// and possibly resetting the head of the bucket (if this was the head). Note that this entry can still be used after being removed to find
		/// the next entry in the list.</summary>
		/// <returns>rue if the removal occurred, or false if the entry had already been removed.</returns>
		public bool remove() {
			if (bucketIndex < 0)	//Already removed
				return false;
			--parent.size;
			if (next != null)
				next.prev = prev;
			if (prev != null)
				prev.next = next;
			else					//We are the first item in the list. Set the bucket's first item to our next item.
				parent.buckets[bucketIndex] = next;
			bucketIndex = -1;		//Mark ourselves as removed.
			return true;
		}
	}
	
	public WeakIdentityDictionary() {
		Clear();
		modCount = 0;
	}
	
	/** Max threshold is the size at which this map's capacity is increased. Hash collisions should be reduced, and memory usage will increase.
	 * Min threshold is the size at which this map's capacity is decreased. Memory usage will be reduced, and hash collisions may increase. */
	private void calculateThreshold() {
		maxThreshold = (buckets.Length * 3) / 4;
		minThreshold = buckets.Length / 4;
	}
	
	/** Reports the quality of the hashcode currently in use. 1 implies no hash collisions, while lower numbers imply more collisions and therefore worse performance overall.
	 * @see #fillRate */
	protected float hashQuality() {
		if (size == 0)
			return 1;
		int fullBuckets = 0;
		for (int i = 0; i < buckets.Length; ++i) {
			if (buckets[i] != null)
				++fullBuckets;
		}
		return (float)fullBuckets / size;
	}
	
	/**
	 * Reports the ratio of filled buckets to total buckets. Maximum value is 0.75 (unless the method to calculate the threshold changes).
	 * Normally the minimum value is 0.25. However, in some circumstances, the value can be less, including:
	 * <ul><li>The map has fewer than 4 items, or</li>
	 * <li>items were removed using the iterator's {@link Iterator#remove()} method, and the map has not had a chance to clean up afterwards.</li></ul>  
	 * <p>
	 * If this this and <code>hashQuality</code> are both high, then the table is doing what it's supposed to be doing and is near the threshold for expanding.
	 * <br>
	 * If this is high but <code>hashQuality</code> is low, then the threshold for either expansion or shrinkage is too high, possibly causing many hash collisions.
	 * <br>
	 * If this is low but <code>hashQuality</code> is high, then the threshold for either expansion or shrinkage is too low, causing unnecessary memory usage.
	 * <br>
	 * If this and <code>hashQuality</code> are both low, you need to reexamine your hashing or mapping function.
	 * @see #hashQuality
	 */
	protected float fillRate() {
		int fullBuckets = 0;
		for (int i = 0; i < buckets.Length; ++i) {
			if (buckets[i] != null)
				++fullBuckets;
		}
		return (float)fullBuckets/buckets.Length;
	}
	
	/**
	 * Reports the number of items in the bucket with the largest number of items.
	 */
	protected int maxBucketSize() {
		int ret = 0;
		foreach (WeakEntry head in buckets) {
			WeakEntry entry = head;
			int depth = 0;
			while (entry != null) {
				++depth;
				entry = entry.next;
			}
			if (depth > ret)
				ret = depth;
		}
		return ret;
	}
	
	protected int sizeOfBucket(int bucket) {
		WeakEntry entry = buckets[bucket];
		int ret = 0;
		while (entry != null) {
			++ret;
			entry = entry.next;
		}
		return ret;
	}
	
	private void setCapacity(int newCapacity)
	{
		WeakEntry[] oldBuckets = buckets;
		size = 0;
		if (!MathUtilities.IsPowerOfTwo(newCapacity))
			throw new ArgumentException();
		buckets = new WeakEntry[newCapacity];
		foreach(WeakEntry head in oldBuckets) {
			WeakEntry entry = head;
			while (entry != null) {
				object key = entry.Target;
				WeakEntry next = entry.next;
				if (key != null) {
					++size;
					entry.setBucketIndex(indexForHash(key));
				} else {
					entry.bucketIndex = -1;
				}
				entry = next;
			}
		}
		calculateThreshold();
	}
	
	/** Double capacity and rehash all entries. */
	private void grow() {
		setCapacity(buckets.Length * 2);
	}
	
	/** Halve capacity and rehash all entries. */
	private void shrink() {
		if (buckets.Length >= INITIAL_BUCKET_COUNT * 2)
			setCapacity(buckets.Length / 2);
	}
	
	/**
	 * Calculates the hash value of the object. Note that as this is an IdentityHashMap, distinct key object instances should generally have a unique value, regardless of equality.
	 * <p>
	 * The default implementation is based on (but not identical to) {@link System#identityHashCode(Object)}.
	 * @param key The object of which the hash should be calculated
	 */
	protected int hash(object key) {			//If you want to provide your own hashing algorithm, go ahead
		int h = RuntimeHelpers.GetHashCode(key);
		
		//The identityHashCode is not particularly good at making an even distribution across the range of values, so we'll fiddle with it a bit.
		h = unchecked((h >> 6) * (int)0x9E3779B9);
		return h;
	}
	
	/**
	 * Translates a hash code into a bucket index. Indices should be spread out across the range [0,<code>length</code>) as evenly as possible,
	 * assuming the input is spread out approximately evenly across the range [{@link Integer#MIN_VALUE}, {@link Integer#MAX_VALUE}].
	 * The same output must always be generated from the same input.
	 * @param hash The hash code to translate
	 * @param length The maximum return value. Always a power of 2.
	 * @return A value in the range [0,<code>length</code>)
	 */
	protected int indexForHash(int hash, int length) {	//If you want to provide your own hash->index algorithm, go ahead
		return hash & (length - 1);
	}
	
	private int indexForHash(object key) {
		return indexForHash(hash(key), buckets.Length);
	}
	
	/** Finds the WeakEntry for the specified key within the bucket at the specified index */
	private WeakEntry findForKey(int index, object key) {
		WeakEntry entry = buckets[index];
		while (entry != null) {
			TKey entryKey = entry.Target as TKey;
			if (entryKey == key)
				return entry;
			else if (entryKey == null)
				entry.remove();
			entry = entry.next;
		}
		return null;
	}
	
	public TValue this[TKey key]
	{
		get
		{
			if (key == null)
				return default(TValue);
			clean();
			int bucketIndex = indexForHash(key);
			WeakEntry entry = findForKey(bucketIndex, key);
			return entry == null ? null : entry.value;
		}
		set
		{
			if (key == null)
				throw new ArgumentNullException();
			clean();
			int bucketIndex = indexForHash(key);
			WeakEntry existingEntry = findForKey(bucketIndex, key);
			if (existingEntry != null) {
				existingEntry.value = value;
			} else {
				new WeakEntry(key, value, bucketIndex, this);
			}
			if (size > maxThreshold)
				grow();
		}
	}
	
	public bool ContainsKey(TKey key) {
		if (key == null)
			return false;
		clean ();
		return findForKey(indexForHash(key), key) != null;
	}
	
	public bool Remove(TKey key)
	{
		if (key == null)
			return false;
		clean();
		int bucketIndex = indexForHash(key);
		TValue ret = null;
		if (buckets[bucketIndex] != null) {
			WeakEntry entry = findForKey(bucketIndex, key);
			if (entry != null) {
				entry.remove();
				ret = entry.value;
			}
		}
		return ret != null;
	}
	
	public int Count { get { return size; } }
	
	
	/** Remove all objects from this map. Essentially has the same effect as creating a new map,
     * with the added benefit of any in-use iterators throwing a ConcurrentModificationException. */
	public void Clear() {
		buckets = new WeakEntry[INITIAL_BUCKET_COUNT];
		calculateThreshold();
		++modCount;
	}
	
	private void clean() {
		foreach (WeakEntry head in buckets) {
			WeakEntry entry = head;
			while (entry != null) {
				if (!entry.IsAlive)
					entry.remove();
				entry = entry.next;
			}
		}
		if (size < minThreshold)
			shrink();
		++modCount;
	}
	
	public override string ToString() {
		StringBuilder sb = new StringBuilder();
		sb.Append("{");
		foreach (WeakEntry head in buckets) {
			WeakEntry entry = head;
			while (entry != null) {
				if (entry.ToString(sb))
					sb.Append(", ");
				else
					entry.remove();
				entry = entry.next;
			}
		}
		sb.Remove(sb.Length - 2, 2);
		sb.Append("}");
		return sb.ToString();
	}
}