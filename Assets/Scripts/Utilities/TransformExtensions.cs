﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions {

    public static void LookAtDir2D (this Transform t, Vector2 dir) {
        t.rotation = Quaternion.identity;
        t.Rotate(0f, 0f, GeometryUtils.RotationToLookAtPoint(t.up, dir));
    }
    
}
