﻿using UnityEngine;
using System.Collections.Generic;


/// <summary>Notifications are sent using NotificationCenter. They generically notify interested parties of something happening.</summary>
public class Notification
{
	/// <summary>The name of the notification. Never null.</summary>
	public string Name { get; private set; }
	/// <summary>The object that sent the notification. May be null.</summary>
	public object Sender { get; private set; }
	/// <summary>Notification-specific set of key/value pairs. May be null.</summary>
	public IDictionary<string, object> UserInfo { get; private set; }
	
	/// <summary>Creates a new notification with no user info.</summary>
	/// <param name="name">Name of the notification. Must not be null.</param>
	/// <param name="sender">Object that is sending the notification.</param>
	public Notification(string name, object sender) : this(name, sender, null) {}
	
	/// <summary>Creates a new notification.</summary>
	/// <param name="name">Name of the notification. Must not be null.</param>
	/// <param name="sender">Object that is sending the notification.</param>
	/// <param name="userInfo">Notification-specific dictionary containing key-value pairings with extra information.</param>
	public Notification(string name, object sender, IDictionary<string, object> userInfo)
	{
		if (name == null)
			throw new System.ArgumentNullException("name");
		Name = name;
		Sender = sender;
		UserInfo = userInfo;
	}
	
	public override string ToString()
	{
		return string.Format("[Notification: Name={0}, Sender={1}, UserInfo={2}]", Name, Sender, UserInfo);
	}
}