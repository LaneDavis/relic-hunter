﻿using System;

public static class DelegateExtensions {
    /// <summary>
    /// Safe to call on null. Invokes this action if it is not null.
    /// </summary>
    public static void SafeInvoke(this Action self) {
        if (self != null) {
            self();
        }
    }
    /// <summary>
    /// Safe to call on null. Invokes this action if it is not null.
    /// </summary>
    public static void SafeInvoke<T1>(this Action<T1> self, T1 obj1) {
        if (self != null)
            self(obj1);
    }
    /// <summary>
    /// Safe to call on null. Invokes this action if it is not null.
    /// </summary>
    public static void SafeInvoke<T1, T2>(this Action<T1, T2> self, T1 obj1, T2 obj2) {
        if (self != null)
            self(obj1, obj2);
    }
}
