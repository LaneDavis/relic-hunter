﻿using UnityEngine;

public static class ParticleSystemExtensions {

    public static void SetMainColor(this ParticleSystem ps, Color color) {
        var mainModule = ps.main;
        var mainModuleStartColor = mainModule.startColor;
        mainModuleStartColor.color = color;
        mainModule.startColor = mainModuleStartColor;
    }
    
    public static void SetStartRotation(this ParticleSystem ps, float angles) {
        var mainModule = ps.main;
        var mainModuleStartRotation = mainModule.startRotation;
        mainModuleStartRotation.constant = angles;
        mainModule.startRotation = mainModuleStartRotation;
    }
    
    public static void SetStartSize(this ParticleSystem ps, float size) {
        var mainModule = ps.main;
        var mainModuleStartSize = mainModule.startSize;
        mainModuleStartSize.constant = size;
        mainModule.startSize = mainModuleStartSize;
    }

    public static void SetStartSpeed(this ParticleSystem ps, float speed) {
        var mainModule = ps.main;
        var mainModuleStartSpeed = mainModule.startSpeed;
        mainModuleStartSpeed.constant = speed;
        mainModule.startSpeed = mainModuleStartSpeed;
    }

    public static void SetStartLifetime(this ParticleSystem ps, float lifeTime) {
        var mainModule = ps.main;
        var mainModuleStartLifetime = mainModule.startLifetime;
        mainModuleStartLifetime.constant = lifeTime;
        mainModule.startLifetime = mainModuleStartLifetime;
    }

    public static void SetConstantEmitRate(this ParticleSystem ps, float rate) {
        var emission = ps.emission;
        var emissionRateOverTime = emission.rateOverTime;
        emissionRateOverTime.constant = rate;
        emission.rateOverTime = emissionRateOverTime;
    }
    
}
