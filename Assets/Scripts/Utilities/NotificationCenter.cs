﻿using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using System;

/// <summary>
/// NotificationCenter is used to send notifications to objects.
/// </summary>
/// <para>Interested parties subscribe to notifications on objects that they are interested in. When those objects post 
/// notifications to this center, objects that have registered to receive notifications are notified.</para>
public class NotificationCenter
{
	/// <summary>
	/// Maps from an observer to a list of all rows that contain that observer. Keys are observers. Values are subscribe references.
	/// </summary>
	private readonly WeakIdentityDictionary<object, List<Row>> observerLookupTable = new WeakIdentityDictionary<object, List<Row>>();
	/// <summary>
	/// List of objects subscribed to all notifications. Values are subscribe references.
	/// </summary>
	private readonly List<Row> noNameNoSender = new List<Row>();
	/// <summary>
	/// Table of objects subscribed to a particular notification but with no sender. Keys are names. Values are subscribe references
	/// </summary>
	private readonly Dictionary<String, List<Row>> noSenderTable = new Dictionary<String, List<Row>>();
	/// <summary>
	/// Table of objects subscribed to a particular sender but no notification name. Keys are senders. Values are subscribe references.
	/// </summary>
	private readonly WeakIdentityDictionary<object, List<Row>> noNameTable = new WeakIdentityDictionary<object, List<Row>>();
	/// <summary>
	/// Table of objects subscribed to a particular notification name from a particular sender. Keys are senders. Values are names/subscribe references.
	/// </summary>
	private readonly WeakIdentityDictionary<object, Dictionary<string, List<Row>>> table = new WeakIdentityDictionary<object, Dictionary<string, List<Row>>>();

	/// <summary>
	/// This delegate method is used to receive notifications.
	/// </summary>
	public delegate void NotificationHandler(Notification note);
	
	private static readonly NotificationCenter defaultCenter;
	
	static NotificationCenter()
	{
		defaultCenter = new NotificationCenter();
	}
	
	/// <summary>
	/// Returns the default notification center. All<sup>*</sup> notifications are sent and received through this center.
	/// </summary>
	/// <para style="font-size:0.75em">* - This is not technically a singleton. You can create your own notification center and
	///	use it to send and receive notifications without using this center if you want.</para>
	public static NotificationCenter DefaultCenter
	{
		get { return defaultCenter; }
	}

	/// <summary>
	/// Adds an observer.
	/// </summary>
	/// <param name="observerMethod">Method to be called when the notification is delivered. Must not be null.</param>
	/// <param name="notificationName">Name of the notification. Pass in null for all notifications.</param>
	/// <param name="notificationSender">Object that is sending. Pass in null for all objects.</param>
	/// <param name="rethrowInvocationExceptions">Sets whether exceptions that happen during the observer method should be rethrown.</param> 
	/// <returns>
	/// An opaque object. Either this object, or the object that has the observerMethod, needs to be strongly referenced 
	/// to guarantee notification delivery. That is, if you do something like this:
	/// <code> universalObservationReceipt = NotificationCenter.DefaultCenter.AddObserver((Notification note) => 
	/// { Console.WriteLine("Notification sent! name={0}, sender={1}, userInfo={2}", note.Name, note.Sender, note.UserInfo); }, null, null); </code>
	/// you need to store the return value (as in the example) and keep that value as long as you want the method to be called. 
	/// (For anonymous methods like this, you can also use the returned object to remove the observer, which is what you should do when 
	/// you want to stop receiving notifications rather than just setting the field to null or something, even though that would also 
	/// eventually stop notifications.) For normal methods, you should ignore the return value.
	/// </returns> 
	public object AddObserver(NotificationHandler observerMethod, string notificationName, object notificationSender, bool rethrowInvocationExceptions = false)
	{
		if (observerMethod == null)
			throw new System.ArgumentNullException();
		
		object notificationObserver = observerMethod.Target; //Marks the object where the method originated as being the observer
		if (notificationObserver == null)
			notificationObserver = observerMethod.Method.DeclaringType; //for if the object is static
		
		Row row = new Row(notificationObserver, observerMethod.Method, notificationName, notificationSender, rethrowInvocationExceptions);
		
		lock(this)
		{
			List<Row> allObservations = observerLookupTable[notificationObserver]; // WeakIdentityDictionary; does not throw for unknown keys.
			if (allObservations == null)
			{
				allObservations = new List<Row>();
				observerLookupTable[notificationObserver] = allObservations;
			}
			allObservations.Add(row);

			//branching if/else statements determine which catagory to put this observer into.
			if (notificationName == null)
			{
				if (notificationSender == null)
				{
					noNameNoSender.Add(row);
				} else {
					List<Row> rows = noNameTable[notificationSender]; // WeakIdentityDictionary; does not throw for unknown keys.
					if (rows == null)
					{
						rows = new List<Row>();
						noNameTable[notificationSender] = rows;
					}
					rows.Add(row);
				}
			} else {
				if (notificationSender == null)
				{
					
					List<Row> rows;
					if (!noSenderTable.TryGetValue(notificationName, out rows))
					{
						rows = new List<Row>();
						noSenderTable[notificationName] = rows;
					}
					rows.Add(row);
				} else {
					Dictionary<string, List<Row>> secondTable = table[notificationSender]; // WeakIdentityDictionary; does not throw for unknown keys.
					if (secondTable == null)
					{
						secondTable = new Dictionary<string, List<Row>>();
						table[notificationSender] = secondTable;
					}
					List<Row> rows;
					if (!secondTable.TryGetValue(notificationName, out rows))
					{
						rows = new List<Row>();
						secondTable[notificationName] = rows;
					}
					rows.Add(row);
				}
			}
		}
		
		return notificationObserver;
	}

	/// <summary>
	/// Posts the notification to the relevant dictionary or list, depending on the properties of the notification. All observers
	/// for this notification are notified.
	/// </summary>
	/// <param name="notification">The notification to be added.</param>
	public void PostNotification(Notification notification)
	{
		string name = notification.Name;
		object sender = notification.Sender;
		
		// Save off rows that need to be posted and do the actual post outside of the synchronized block.
		// This also allows people to remove an observer while being notified.
		List<Row> rowsToPost = new List<Row>();
		lock(this)
		{
			rowsToPost.AddRange(noNameNoSender);
			List<Row> rows;

			//branching if statements determine which catagories to post this notification into.
			if (noSenderTable.TryGetValue(name, out rows))
				rowsToPost.AddRange(rows);
			if (sender != null)
			{
				rows = noNameTable[sender];
				if (rows != null)
					rowsToPost.AddRange(rows);
				
				Dictionary<string, List<Row>> secondTable = table[sender];
				if (secondTable != null && secondTable.TryGetValue(name, out rows))
				{
					rowsToPost.AddRange(rows);
				}
			}
		}
		foreach(Row r in rowsToPost)
			r.Post(notification);
	}

	/// <summary>
	/// Posts the notification to the relevant dictionary or list, depending on the properties of the notification. All observers
	/// for this notification are notified.
	/// </summary>
	/// <param name="notificationName">The name of the notification to be posted.</param>
	/// <param name="notificationSender">The object sending the notification to be posted.</param>
	public void PostNotification(string notificationName, object notificationSender)
	{
		PostNotification(new Notification(notificationName, notificationSender));
	}

	/// <summary>
	/// Posts the notification to the relevant dictionary or list, depending on the properties of the notification. All observers
	/// for this notification are notified.
	/// </summary>
	/// <param name="notificationName">The name of the notification to be posted.</param>
	/// <param name="notificationSender">The object sending the notification to be posted.</param>
	/// <param name="userInfo">Notification-specific dictionary containing key-value pairings with extra information.</param>
	public void PostNotification(string notificationName, object notificationSender, Dictionary<string, object> userInfo)
	{
		PostNotification(new Notification(notificationName, notificationSender, userInfo));
	}
	
	/// <summary>
	/// Posts the notification on the main thread. If this is called from the main thread, the notification will be sent to 
	/// observers immediately (i.e. before this method returns.)
	/// </summary>
	/// <param name="note">Note to post.</param>
	public void PostNotificationOnMainThread(Notification note)
	{
		if (ThreadingUtil.Instance.IsMainThread)
			PostNotification(note);
		else
			ThreadingUtil.Instance.RunOnUnityMainThread(()=>PostNotification(note));
	}
	
	/// <summary>
	/// Posts a notification on the main thread. If this is called from the main thread, the notification will be sent to 
	/// observers immediately (i.e. before this method returns.)
	/// </summary>
	/// <param name="notificationName">Notification name.</param>
	/// <param name="notificationSender">Notification sender.</param>
	/// <param name="userInfo">User info.</param>
	public void PostNotificationOnMainThread(string notificationName, object notificationSender, Dictionary<string, object> userInfo = null)
	{
		PostNotificationOnMainThread(new Notification(notificationName, notificationSender, userInfo));
	}

	/// <summary>
	/// Removes all instances of an observer for all notifications.
	/// </summary>
	/// <param name="notificationObserver">The observer to remove.</param>
	public void RemoveObserver(object notificationObserver)
	{
		RemoveObserver(notificationObserver, null, null);
	}
		
	/// <summary>
	/// Removes the observer using the specified criteria.
	/// </summary>
	/// <param name="notificationObserver">observer to remove. Usually you pass "this." If you used a delegate method (e.g., you 
	/// passed in (Notification n)=>{...}) then use the object returned by AddObserver. If you passed in a static method, use the 
	/// type of the class that declared the static method.</param>
	/// <param name="notificationName">name of observed notifications to remove, or null to specify removal from all 
	/// notification names. </param>
	/// <param name="notificationSender">object to stop observing, or null to stop observing all objects.</param>
	public void RemoveObserver(object notificationObserver, string notificationName, object notificationSender)
	{
		lock(this)
		{
			List<Row> allRows = observerLookupTable[notificationObserver];
			List<Row> doomedRows = null;
			if (allRows != null)
			{
				foreach(Row r in allRows)
				{
					object strongSender;
					if ((notificationSender == null || 
					     (r.sender != null && (((strongSender = r.sender.Target) == notificationSender) || strongSender == null))) &&
					    (notificationName == null || 
					 (r.name != null && r.name.Equals(notificationName))))
					{
						if (doomedRows == null)
							doomedRows = new List<Row>();
						doomedRows.Add(r);
					}
				}
				
				if (doomedRows != null)
				{
					allRows.RemoveAll((Row r) => doomedRows.Contains(r));
					foreach(Row r in doomedRows)
						removeRow(r);
					
					if (allRows.Count == 0)
						observerLookupTable.Remove(notificationObserver);
				}
			}
		}
	}

	/// <summary>
	/// Removes a row from the notification center. This should only be called when synchronized.
	/// </summary>
	/// <param name="r">The row to be removed</param>
	private void removeRow(Row r)
	{
		object strongObserver = r.observer.Target;
		if (strongObserver != null)
		{
			List<Row> rows = observerLookupTable[strongObserver];
			if (rows != null)
			{
				rows.Remove(r);
				if (rows.Count == 0)
					observerLookupTable.Remove(strongObserver);
			}
		}

		//Branching if/else statements determine which catagories Row r needs to be removed from.
		if (r.sender == null)
		{
			if (r.name == null)
			{
				noNameNoSender.Remove(r);
			} else {
				List<Row> noSenderRows;
				if (noSenderTable.TryGetValue(r.name, out noSenderRows))
				{
					noSenderRows.Remove(r);
					if (noSenderRows.Count == 0)
						noSenderTable.Remove(r.name);
				} else
					/*Asserts.CSAssert(false, */Debug.LogError("Inconsistency detected in Notification Center: active observer not present in noSenderTable for " + r.name);
			}
		} else {
			object strongSender = r.sender.Target;
			if (strongSender == null)
			{
				// cannot possibly be any matching entries, because the sender that we registered with has already been collected.
			} else {
				if (r.name == null)
				{
					List<Row> noNameRows = noNameTable[strongSender];
					if (noNameRows != null)
					{
						noNameRows.Remove(r);
						if (noNameRows.Count == 0)
							noNameTable.Remove(strongSender);
					} else
						/*Asserts.CSAssert(false, */Debug.LogError("Inconsistency detected in Notification Center: active observer not present in noNameTable for " + strongSender);
				} else {
					Dictionary<string, List<Row>> senderNameToListTable = table[strongSender];
					if (senderNameToListTable == null)
					{
						/*Asserts.CSAssert(false, */Debug.LogError("Inconsistency detected in Notification Center: active observer not present in table for "+strongSender+": " + r.name);
					} else {
						List<Row> rows;
						if (senderNameToListTable.TryGetValue(r.name, out rows))
						{
							rows.Remove(r);
							if (rows.Count == 0)
							{
								senderNameToListTable.Remove(r.name);
								if (senderNameToListTable.Count == 0)
									table.Remove(strongSender);
							}
						} else {
							/*Asserts.CSAssert(false, */Debug.LogError("Inconsistency detected in Notification Center: active observer not present in senderNameToListTable for " + strongSender + ": " + r.name);
						}
					}
				}
			}
		}
	}

	/// <summary>
	/// Stores information of an object subscribed to particular notifications.
	/// </summary>
	private class Row
	{
		public WeakReference observer;
		public MethodInfo selector;
		public string name;
		public WeakReference sender;
		public bool RethrowInvocationExceptions;
		
		public Row(object notificationObserver, MethodInfo notificationSelector, string notificationName, object notificationSender, bool rethrowExceptions)
		{
			observer = new WeakReference(notificationObserver);
			selector = notificationSelector;
			name = notificationName;
			sender = notificationSender==null?null:new WeakReference(notificationSender);
			RethrowInvocationExceptions = rethrowExceptions;
		}

		/// <summary>
		/// Post the specified note.
		/// </summary>
		/// <param name="note">Note.</param>
		public void Post(Notification note)
		{
			object strongObserver = observer.Target;
			if (strongObserver != null)
			{
				try
				{
					selector.Invoke(strongObserver, new object[] {note});
				} catch (TargetException e) { // should not happen.
					UnityEngine.Debug.LogError(e);
				} catch (ArgumentException e) { // should not happen.
					UnityEngine.Debug.LogError(e);
				} catch (TargetParameterCountException e) { // should not happen.
					UnityEngine.Debug.LogError(e);
				} catch (MethodAccessException e) { // ??? I don't think this should happen, but I'm not sure.
					UnityEngine.Debug.LogError(e);
				} catch (InvalidOperationException e) { // should not happen
					UnityEngine.Debug.LogError(e);
				} catch (NotSupportedException e) { // should not happen
					UnityEngine.Debug.LogError(e);
				} catch (TargetInvocationException e) {
					// underlying method threw an exception.
					// TODO: we were handling it like this in java, and it would probably make sense to do something like this again.
					//ReflectionUtils.InvocationStackException.handleInvocationException(e, "NotificationCenter", "Exception raised during posting of notification %s", note.getName());
					UnityEngine.Debug.LogError("Exception raised during posting of notification " + note.Name);
					UnityEngine.Debug.LogError(e.GetBaseException());
					if (RethrowInvocationExceptions) {
						throw e.GetBaseException();
					}
				}
			}
		}
	}
}
