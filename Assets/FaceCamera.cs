﻿using UnityEngine;

[ExecuteInEditMode]
public class FaceCamera : MonoBehaviour {

    private void Update() {
        if (MainCamera.Camera.orthographic) {
            transform.rotation = MainCamera.GameObject.transform.rotation;
        }
        else {
            transform.LookAt(MainCamera.GameObject.transform);
        }
    }
    
}
