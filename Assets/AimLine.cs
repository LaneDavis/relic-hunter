﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimLine : MonoBehaviour {

    public Transform LineTransform;
    public SpriteRenderer LineRenderer;
    public float LineLengthMax;

    private Quaternion baseRotation;

    private void Awake() {
        baseRotation = transform.localRotation;
    }
    
    public void Aim(Vector2 inputVec) {

        float mag = inputVec.magnitude * LineLengthMax;
        
        Debug.DrawLine(transform.position, transform.position + Vector3.right * 15f);
        
        LineRenderer.size = new Vector2(mag / LineTransform.localScale.x, LineRenderer.size.y);
        LineTransform.localPosition = new Vector3(mag / 2f + 1.25f, LineTransform.localPosition.y, LineTransform.localPosition.z);

        transform.rotation = baseRotation;
        transform.Rotate(0f, 0f, GeometryUtils.RotationToLookAtPoint(Vector3.right, inputVec));

    }

}
