﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    Animator Animator;
    public GameObject Boss;
    // Start is called before the first frame update
    void Start()
    {
        Animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
       
        if(Boss != null)
        {
            Debug.Log("Works");
            Animator.SetTrigger("Door Open");

        }
    }

    private void OnTriggerExit(Collider other)
    {
        
        if (Boss != null)
        {

            Animator.SetTrigger("Door Close");

        }

    }


}
