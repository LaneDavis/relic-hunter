﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Win32.SafeHandles;
using Rewired.UI.ControlMapper;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CustomGravity))]
[RequireComponent(typeof(BuffController))]
public class EntityController : MonoBehaviour {
    public EntityBaseStats BaseStats;
    public HealthBar healthBar;

    [HideInInspector] public EntityData EntityData;
    
    // Anatomy
    private Rigidbody rb;
    public AttackController ActiveAttack;
    private BuffController buffController;

    // Movement
    public Action<Vector2> OnMove;

    // Jumping
    [Header("Jump")]
    public AnimationCurve JumpStrengthOverTime;
    public float JumpStrengthInitial = 1.5f;
    private int JumpsLeft;
    public float GravNormal = 8f;
    public float GravJumping = 5f;
    private bool startedAJump;
    private bool stoppedAJump;
    private bool isJumping;
    private bool touchedGroundThisFrame;
    private float jumpTimer = 0f;
    private CustomGravity customGravity;
    
    // Animations
    [Header("Animations")]
    public CharacterAnimations Animations;
    public Animator SpecialAnimatorLeft;
    public Animator SpecialAnimatorRight;
    
    // Attachments
    private List<EntityAttachment> ActiveAttachments = new List<EntityAttachment>();
    
    // Taking Damage
    [Header("Taking Damage")]
    public float Hazard_TickTime = 0.5f;
    private float Hazard_TickTimer;
    public float Hazard_SafetyTime = 0.1f;
    private float Hazard_SafetyTimer;

    // DOTS
    public float DOT_TickTime = 0.5f;
    private float DOT_TickTimer;
    public delegate void DOT_Delegate(EntityController entityController);
    public event DOT_Delegate OnDOTTick;
    
    // Player Only
    [HideInInspector] public PlayerData PlayerData;
    [HideInInspector] public SpecialController LeftSpecial;
    [HideInInspector] public SpecialController RightSpecial;
    
    // Terrain Damage
    private List<Collision> HazardCollisions = new List<Collision>();
    
    // Death
    public delegate void DeathDelegate(EntityController thisEntityController);
    public event DeathDelegate OnDeath;
    
    private void Awake() {
        rb = GetComponent<Rigidbody>();
        customGravity = GetComponent<CustomGravity>();
    }

    private void Start() {
        buffController = GetComponent<BuffController>();
        EntityData = new EntityData(BaseStats, GetComponentsInChildren<Collider>().ToList(), buffController);
        EntityData.EntityObject = gameObject;

        if (BaseStats is PlayerBaseStats) {
            SetupPlayer();
        }
    }

    private void SetupPlayer() {
        PlayerBaseStats playerStats = (PlayerBaseStats) BaseStats;

        Transform t = transform;
        
        // Setup Attack
        if (playerStats.AttackPrefab != null) {
            GameObject attackObject = Instantiate(playerStats.AttackPrefab, t.position, t.rotation, t);
            ActiveAttack = attackObject.GetComponent<AttackController>();
        }

        // Setup Specials
        if (playerStats.LeftSpecialPrefab != null) {
            GameObject leftSpecialObject = Instantiate(playerStats.LeftSpecialPrefab, t.position, t.rotation, t);
            LeftSpecial = leftSpecialObject.GetComponent<SpecialController>();
            LeftSpecial.Init(SpecialAnimatorLeft);
        }
        
        if (playerStats.RightSpecialPrefab != null) {
            GameObject rightSpecialObject = Instantiate(playerStats.RightSpecialPrefab, t.position, t.rotation, t);
            RightSpecial = rightSpecialObject.GetComponent<SpecialController>();
            RightSpecial.Init(SpecialAnimatorRight);
        }

        SpriteFacingController sfc = GetComponentInChildren<SpriteFacingController>();
        if (sfc != null) {
            if (playerStats.AwaySprite != null) sfc.SpriteAway = playerStats.AwaySprite;
            if (playerStats.TowardSprite != null) sfc.SpriteToward = playerStats.TowardSprite;
            if (playerStats.LeftSprite != null) sfc.SpriteLeft = playerStats.LeftSprite;
            if (playerStats.RightSprite != null) sfc.SpriteRight = playerStats.RightSprite;
        }
        
    }
    
    public void Move(Vector2 moveVector) {
        if (EntityData.Buff_SpeedMultiplier < 0.9f) {
            Debug.Log("Gotcha.");
        }
        moveVector *= EntityData.Buff_SpeedMultiplier;
        rb.velocity = new Vector3(moveVector.x * BaseStats.moveSpeed, rb.velocity.y, moveVector.y * BaseStats.moveSpeed);
        OnMove.SafeInvoke(moveVector);
    }

    public void Jump() {
        if (!isJumping) {startedAJump = true;}
    }

    public void StopJump() {
        if (isJumping) stoppedAJump = true;
    }

    private void Update () {

        UpdateJump();
        UpdateHazards();
        UpdateDOTs();
        
        if (transform.position.y < KillFloor.KillFloorHeight) {
            Die();
        }
        
        healthBar.SetHP(EntityData.CurHP / EntityData.MaxHP);
        
    }

    private void LateUpdate() {
        touchedGroundThisFrame = false;
    }

    public void TakeDamage(float damageAmount) {

        EntityData.CurHP -= damageAmount;
        
        // Do damage feedback effects.
        float impactMagnitude = Mathf.Clamp01(damageAmount / EntityData.MaxHP * 3f);
        Animations.OnTakeDamage(impactMagnitude);
        if (PlayerData != null){
            PlayerData.AddRumble(impactMagnitude);
        }
        if (EntityData.CurHP <= 0f) {
            Die();
        }
        
    }

    public void AddAttachment(GameObject AttachmentPrefab) {
        EntityAttachment hypotheticalNewAttachment = AttachmentPrefab.GetComponent<EntityAttachment>();
        if (hypotheticalNewAttachment == null) return;
        foreach (EntityAttachment oldAttachment in ActiveAttachments) {
            if (!oldAttachment.CanMakeNewAttachmentCheck(hypotheticalNewAttachment)) ;
            return;
        }
        
        GameObject newAttachmentObject = Instantiate(AttachmentPrefab, transform.position, Quaternion.identity, transform);
        EntityAttachment newAttachment = newAttachmentObject.GetComponent<EntityAttachment>();
        newAttachment.OnAttached(this);
        ActiveAttachments.Add(newAttachment);
    }

    public void UnsubscribeAttachment(EntityAttachment oldAttachment) {
        ActiveAttachments.Remove(oldAttachment);
    }

    private void UpdateDOTs() {

        DOT_TickTimer += Time.deltaTime;
        if (DOT_TickTimer > DOT_TickTime) {
            DOT_TickTimer = 0f;
            OnDOTTick?.Invoke(this);
        }

    }
    
    private void UpdateHazards() {

        if (Hazard_TickTimer >= Hazard_TickTime) {
            Hazard_TickTimer = 0f;
        }
        
        if (HazardCollisions.Count > 0) {
            Hazard_SafetyTimer = 0f;
            
            if (Hazard_TickTimer <= 0f) {
                TakeDamage(60f);
            }

            Hazard_TickTimer += Time.fixedDeltaTime;
        }
        else {
            Hazard_SafetyTimer += Time.deltaTime;
            if (Hazard_SafetyTimer >= Hazard_SafetyTime) {
                Hazard_TickTimer = 0f;
            }
        }
        
        HazardCollisions.Clear();
        
    }
    
    private void UpdateJump() {
        
        // Start Jump
        if (startedAJump) {
            startedAJump = false;
            if (JumpsLeft > 0) {
                Animations.OnJump();
                rb.velocity = new Vector3(rb.velocity.x, JumpStrengthInitial, rb.velocity.z);
                isJumping = true;
                jumpTimer = 0f;
                JumpsLeft--;
            }
        }

        if (isJumping) {
            rb.velocity += new Vector3(0f, JumpStrengthOverTime.Evaluate(jumpTimer) * Time.deltaTime, 0f);
            customGravity.gravityScale = GravJumping;
            jumpTimer += Time.deltaTime;
        }
        else {
            customGravity.gravityScale = GravNormal;
        }

        if (stoppedAJump) {
            stoppedAJump = false;
            isJumping = false;
        }

    }
    
    // Reset Jumps
    public void ResetJumpsIfOnFloor(Collision collision) {
        if ((collision.gameObject.layer == 9 /*Terrain*/ || collision.gameObject.layer == 11 /*Terrain, Murder*/) && transform.position.y > collision.transform.position.y && !isJumping) {
            JumpsLeft = BaseStats.JumpCount;
        }
    }

    // Detect
    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.layer == 11) {
            HazardCollisions.Add(collision);
        }
        if (collision.gameObject.layer == 13 && collision.gameObject.GetComponent <EntityController>() && collision.gameObject.GetComponent<EntityController>().BaseStats.Hazardous && collision.gameObject.tag != gameObject.tag)
        {
            HazardCollisions.Add(collision);
        }

        ResetJumpsIfOnFloor(collision);
        touchedGroundThisFrame = true;
    }

    // Detect
    void OnCollisionStay(Collision collision) {
        if (collision.gameObject.layer == 11) {
            HazardCollisions.Add(collision);
        }
        ResetJumpsIfOnFloor(collision);
        touchedGroundThisFrame = true;
    }
    
    // Left Special
    public void StartLeftSpecial(Vector2 aimVector) {
        if (LeftSpecial != null) {
            LeftSpecial.StartSpecial(BuildSpecialData(aimVector));
        }
    }
    
    public void HoldLeftSpecial(Vector2 aimVector) {
        if (LeftSpecial != null) {
            LeftSpecial.HoldSpecial(BuildSpecialData(aimVector));
        }
    }
    
    public void ReleaseLeftSpecial(Vector2 aimVector) {
        if (LeftSpecial != null) {
            LeftSpecial.ReleaseSpecial(BuildSpecialData(aimVector));
        }
    }
    
    // Right Special
    public void StartRightSpecial(Vector2 aimVector) {
        if (RightSpecial != null) {
            RightSpecial.StartSpecial(BuildSpecialData(aimVector));
        }
    }
    
    public void HoldRightSpecial(Vector2 aimVector) {
        if (RightSpecial != null) {
            RightSpecial.HoldSpecial(BuildSpecialData(aimVector));
        }
    }
    
    public void ReleaseRightSpecial(Vector2 aimVector) {
        if (RightSpecial != null) {
            RightSpecial.ReleaseSpecial(BuildSpecialData(aimVector));
        }
    }
    
    // Attack
    public void StartAttack(Vector2 aimVector) {
        if (ActiveAttack != null) {
            ActiveAttack.StartAttack(BuildAttackData(aimVector));
        }
    }
    
    public void HoldAttack (Vector2 aimVector) {
        if (ActiveAttack != null) {
            ActiveAttack.HoldAttack(BuildAttackData(aimVector));
        }
    }
    
    public void ReleaseAttack (Vector2 aimVector) {
        if (ActiveAttack != null) {
            ActiveAttack.ReleaseAttack(BuildAttackData(aimVector));
        }
    }

    private AttackData BuildAttackData(Vector2 aimVector) {
        return new AttackData() { OwnerPlayerData = PlayerData, OwnerEntityData = EntityData, AimVector2D = aimVector };
    }

    private SpecialData BuildSpecialData(Vector2 aimVector) {
        return new SpecialData() { OwnerPlayerData = PlayerData, OwnerEntityData = EntityData, AimVector2D = aimVector };
    }
    
    // Die
    public void Die() {
        OnDeath?.Invoke(this);
        Destroy(gameObject);
        
        
    }
    
}
