﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
[System.Serializable]
public class WaveSpawner : MonoBehaviour
{
    



    public Vector3 center;
    public Vector3 size;

    [System.Serializable]
    public enum SpawnState { SPAWNING, WAITING, COUNTING };


    
    [Serializable]
    public class Wave
        
    {
        
        public string name;
        public GameObject enemy;
        public int count;
        public float spawnrate;

      

        [HideInInspector] public SpawnState state = SpawnState.COUNTING;



    }

    public Wave[] waves;
    private int nextWave = 0;

    private float serchCountdown = 1f;

    public float timeBetweenWaves = 5f;
    public float waveCountdown;

    private SpawnState state = SpawnState.COUNTING;

    [Header("Spawn Height Checking")]
    public LayerMask SpawnLayerMask;
    public float RaycastStartHeight = 10f;
    public float RaycastLength = 100f;

    

    private void Start()
    {

        waveCountdown = timeBetweenWaves;


    }

    private void Update() {
        
        if (state == SpawnState.WAITING) {

            //Check If enemy's are still alive
            if (!EnemyIsAlive()) {

                //Begin New Round
                WaveCompleted();
                return;
            }
            else {

                return;

            }

        }
        
        if (waveCountdown <= 0)
        {

            if (state != SpawnState.SPAWNING)
            {

                StartCoroutine( SpawnWave (waves[nextWave]));

            }
            

        }
        else
        {

            waveCountdown -= Time.deltaTime;

        }

    }

    void WaveCompleted()
    {

       // Debug.Log("Wave Completed!");

        state = SpawnState.COUNTING;
        waveCountdown = timeBetweenWaves;
        if (nextWave +1 > waves.Length - 1)
        {

            nextWave = 0;
         //   Debug.Log("All Waves Completed!!!! Prepare, Looping!");

        }
        else
        {

            nextWave++;

        }

    }


     

    bool EnemyIsAlive()
    {
        serchCountdown -= Time.deltaTime;
        if (serchCountdown <= 0f) 
        {
            serchCountdown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null)
            {
                return false;
            }
        }
        return true;
    }
        
    IEnumerator SpawnWave(Wave _wave)
    {
     //   Debug.Log("Spawning Wave:" + _wave.name);
        state = SpawnState.SPAWNING;

       for (int i = 0; i < _wave.count; i++)
        {

            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds(1f / _wave.spawnrate);

        }

        state = SpawnState.WAITING;

        yield break;

    }
  //  public IEnumerator HoldNavAgent() { yield return new WaitForSeconds(0.1f); pathFinder.enabled = true; target = GameObject.FindGameObjectWithTag("NavTarget"); pathFinder.speed = speed; pathFinder.SetDestination(target.transform.position); }
    void SpawnEnemy (GameObject _enemy) {

        Vector3 spawnPos = transform.position;
        float enemyHeight = 0f;
        SphereCollider enemyBodyCollider = _enemy.GetComponent<SphereCollider>();
        if (enemyBodyCollider != null) {
            enemyHeight = enemyBodyCollider.radius;
        }

        Vector3 RaycastStartPoint = transform.position + Vector3.up * RaycastStartHeight;
        RaycastHit hitInfo;
        Physics.Raycast(RaycastStartPoint, RaycastStartPoint - Vector3.up * RaycastLength, out hitInfo, SpawnLayerMask);
        if (hitInfo.point != default(Vector3)) {
            spawnPos = hitInfo.point /*+ Vector3.up * enemyHeight*/;
        }

        //Spawn Enemy
        GameObject newEnemy = Instantiate(_enemy, spawnPos + Vector3.up * 100f, transform.rotation);
        ThreadingUtil.Instance.RunLater(() => {
            newEnemy.transform.position = spawnPos;
            IEnemyInput enemyInput = newEnemy.GetComponent<IEnemyInput>();
            if (enemyInput != null) {
                enemyInput.WarpToMesh(spawnPos);
            }
        });
        // Debug.Log("Spawning Enemy: " + _enemy.name);



    }
 

    private void OnDrawGizmosSelected()
    {

        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(center, size);
    }


}
