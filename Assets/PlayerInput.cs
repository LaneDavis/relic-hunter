﻿using Rewired;
using UnityEngine;

[RequireComponent(typeof(EntityController))]
public class PlayerInput : MonoBehaviour {

    // Data
    [HideInInspector] public PlayerData PlayerData;
    
    // Anatomy
    private EntityController controller;
    public AimLine AimLine;

    private void Awake() {
        controller = GetComponent<EntityController>();
    }
    
    private void Update() {
        
        // Handle Move Inputs.
        Vector2 moveInputVec = new Vector2(PlayerData.RewiredPlayer.GetAxisRaw("Move Horizontal"), PlayerData.RewiredPlayer.GetAxisRaw("Move Depth"));
        if (moveInputVec.magnitude > 1f) moveInputVec = moveInputVec.normalized;
        Vector2 moveWorldVec = MainCamera.Cam2World(moveInputVec);
        controller.Move(moveWorldVec);
        
        // Handle Aim Inputs.
        Vector2 aimInputVec = new Vector2(PlayerData.RewiredPlayer.GetAxisRaw("Aim Horizontal"), PlayerData.RewiredPlayer.GetAxisRaw("Aim Depth"));
        if (aimInputVec.magnitude > 1f) aimInputVec = aimInputVec.normalized;
        Vector2 inputWorldVec = MainCamera.Cam2World(aimInputVec);
        AimLine.Aim(inputWorldVec);
        
        // Handle Attack Inputs.
        if (PlayerData.RewiredPlayer.GetButtonDown("Attack")) { controller.StartAttack(inputWorldVec);}
        if (PlayerData.RewiredPlayer.GetButton("Attack")) {controller.HoldAttack(inputWorldVec);}
        if (PlayerData.RewiredPlayer.GetButtonUp("Attack")) {controller.ReleaseAttack(inputWorldVec);}
        
        // Handle Special Inputs
        if (PlayerData.RewiredPlayer.GetButtonDown("Left Special")) { controller.StartLeftSpecial(inputWorldVec);}
        if (PlayerData.RewiredPlayer.GetButton("Left Special")) {controller.HoldLeftSpecial(inputWorldVec);}
        if (PlayerData.RewiredPlayer.GetButtonUp("Left Special")) {controller.ReleaseLeftSpecial(inputWorldVec);}
        if (PlayerData.RewiredPlayer.GetButtonDown("Right Special")) { controller.StartRightSpecial(inputWorldVec);}
        if (PlayerData.RewiredPlayer.GetButton("Right Special")) {controller.HoldRightSpecial(inputWorldVec);}
        if (PlayerData.RewiredPlayer.GetButtonUp("Right Special")) {controller.ReleaseRightSpecial(inputWorldVec);}
        
        // Handle Jump Inputs.
        if (PlayerData.RewiredPlayer.GetButtonDown("Jump")) {
            controller.Jump();
        }

        if (PlayerData.RewiredPlayer.GetButtonUp("Jump")) {
            controller.StopJump();
        }
        
    }
    
}
