﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Entity Stats/Player Base Stats")]
public class PlayerBaseStats : EntityBaseStats {

    public string DisplayName;
    
    public Color ClassColor;
    public Sprite ClassIcon;
    public float RespawnTimeBase = 8f;
    public float RespawnTimePerDeath = 4f;

    public GameObject AttackPrefab;

    public GameObject LeftSpecialPrefab;
    public GameObject RightSpecialPrefab;

    [Header("Sprites")]
    public Sprite TowardSprite;
    public Sprite AwaySprite;
    public Sprite LeftSprite;
    public Sprite RightSprite;

}
