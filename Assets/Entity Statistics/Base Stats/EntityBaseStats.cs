﻿using UnityEngine;

public class EntityBaseStats : ScriptableObject {

    public float MaxHP = 100f;
    public float moveSpeed = 5f;
    public int JumpCount = 1;
    public bool Hazardous;

}
