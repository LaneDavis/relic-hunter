﻿using System.Collections.Generic;
using System.Linq;
using Rewired;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour {

    public static PlayerManager Instance;
    public GameObject PlayerPrefab;
    public List<PlayerBaseStats> PlayerClasses = new List<PlayerBaseStats>();

    public List<Transform> PlayerSpawnPoints = new List<Transform>();
    public RumbleManager RumbleManager;

    private List<PlayerData> PlayerDatas = new List<PlayerData>();

    [Header("Defeat")]
    public FxController DefeatFX;
    public AnimationCurve DefeatPowerCurve;
    public float DefeatTime = 2f;
    private float defeatTimer;
    private float DefeatProgress => defeatTimer / DefeatTime;
    
    private bool playersHaveEverLived;
    [HideInInspector] public bool IsDoingDefeat;

    [Header("Victory")]
    public FxController VictoryFX;
    public AnimationCurve VictoryPowerCurve;
    public GameObject VictoryText;
    public float VictoryTime = 2f;
    private float victoryTimer;
    private float VictoryProgress => victoryTimer / VictoryTime;
    
    [HideInInspector] public bool IsDoingVictory;
    
    private void Awake() {

        // Setup Singleton
        Instance = this;
        
        // Initialize Player Slots
        for (int i = 0; i < 4; i++) {
            
            // Initialize Player Slots
            PlayerData pd = new PlayerData();
            pd.PlayerNumber = i;
            PlayerDatas.Add(pd);
            RumbleManager.AddPlayerData(pd);
            
            // Initialize Rewired Players
            pd.RewiredPlayer = ReInput.players.GetPlayer(i);

        }

    }

    private void Update() {
        
        // Prompt Class Selection if Appropriate
        for (int i = 0; i < 4; i++) {
            if (PlayerDatas[i].RewiredPlayer.GetAnyButtonDown()) {
                if (PlayerDatas[i].PlayerObject == null && !PlayerDatas[i].IsClassSelecting && !PlayerDatas[i].IsDead) {
                    ClassSelectionController.ClassSelectionControllers[i].Init(PlayerDatas[i], PlayerClasses);
                    PromptClassSelection(PlayerDatas[i]);
                }
            }
        }
        
        // Respawn Dead Players
        for (int i = 0; i < 4; i++) {
            if (PlayerDatas[i].IsDead && Time.time > PlayerDatas[i].RespawnTime) {
                SpawnPlayer(PlayerDatas[i], PlayerDatas[i].BaseStats);
            }
        }
        
        // Trigger defeat if no players are alive
        if (playersHaveEverLived && !IsDoingDefeat && PlayerObjects().Count == 0) {
            IsDoingDefeat = true;
        }

        // Illustrate Defeat
        if (IsDoingDefeat) {
            defeatTimer += Time.deltaTime;
            DefeatFX.Toggle(true, gameObject, DefeatPowerCurve.Evaluate(DefeatProgress));
            if (DefeatProgress >= 1f) SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        
        // Illustrate Victory
        if (IsDoingVictory) {
            victoryTimer += Time.deltaTime;
            VictoryFX.Toggle(true, gameObject, VictoryPowerCurve.Evaluate(VictoryProgress));
            if (VictoryProgress >= 1f) SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    }

    private void PromptClassSelection(PlayerData playerData) {
        playerData.IsClassSelecting = true;
        ClassSelectionController.ClassSelectionControllers[playerData.PlayerNumber].StartClassSelection();
    }

    public void SpawnPlayer(PlayerData playerData, EntityBaseStats baseStats) {

        playersHaveEverLived = true;
        
        GameObject newPlayerObject = Instantiate(PlayerPrefab, PlayerSpawnPoints[Random.Range(0, PlayerSpawnPoints.Count)].position, Quaternion.identity, null);
        playerData.PlayerObject = newPlayerObject;
        playerData.Controller = newPlayerObject.GetComponent<EntityController>();
        playerData.Controller.BaseStats = baseStats;
        playerData.BaseStats = baseStats;
        playerData.Controller.PlayerData = playerData;
        playerData.PlayerInput = newPlayerObject.GetComponent<PlayerInput>();
        playerData.PlayerInput.PlayerData = playerData;
        playerData.Color = ((PlayerBaseStats) playerData.BaseStats).ClassColor;

        RespawnBarController.RespawnBarControllers[playerData.PlayerNumber].Init(playerData);
        playerData.RespawnBarController = RespawnBarController.RespawnBarControllers[playerData.PlayerNumber];
        playerData.Controller.OnDeath += HandlePlayerDeath;
        playerData.IsDead = false;

    }

    public List<GameObject> PlayerObjects() {
        List<GameObject> ret = new List<GameObject>();
        foreach (PlayerData pd in PlayerDatas) {
            if (pd.PlayerObject != null) ret.Add(pd.PlayerObject);
        }
        return ret;
    }

    public PlayerData PlayerDataFromObject(GameObject obj) {
        foreach (PlayerData pd in PlayerDatas) {
            if (pd.PlayerObject == obj) return pd;
        }
        return null;
    }

    public PlayerData NearestPlayerToPoint(Vector3 point) {
        List<GameObject> playerObjects = PlayerObjects();
        if (playerObjects.Count == 0) return null; 

        return PlayerDataFromObject(playerObjects.OrderBy(player => Vector3.Distance(point, player.transform.position)).First());
    }

    private void HandlePlayerDeath(EntityController controller) {
        PlayerData playerData = controller.PlayerData;
        PlayerBaseStats playerBaseStats = (PlayerBaseStats) controller.BaseStats;
        playerData.RespawnTime = Time.time + playerBaseStats.RespawnTimeBase + playerBaseStats.RespawnTimePerDeath * playerData.DeathCount;
        playerData.IsDead = true;
        playerData.DeathCount++;
    }

    public List<PlayerData> AllPlayers () {
        return PlayerDatas.ToList();
    }

    public void ReportVictory() {
        if (IsDoingDefeat) return;
        if (IsDoingVictory) return;
        IsDoingVictory = true;
        VictoryText.SetActive(true);
    }
    
}
