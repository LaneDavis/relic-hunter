﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyController : MonoBehaviour
{

    Transform target;
    NavMeshAgent agent;


    public float lookradious = 10f;

    // Start is called before the first frame update
    void Start() {
        agent = GetComponent<NavMeshAgent>();

    }

    // Update is called once per frame
    void Update() {

        // Target the nearest player.
        PlayerData targetPlayer = PlayerManager.Instance.NearestPlayerToPoint(transform.position);
        if (targetPlayer == null) return;
        target = targetPlayer.PlayerObject.transform;
        
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance <= lookradious)
        {

            agent.SetDestination(target.position);

        }
    }

    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookradious);

    }



}


