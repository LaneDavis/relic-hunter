﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scout : MonoBehaviour {
    private float speed = 5f;
    public Transform[] moveSpots;
    private int randomSpot;
    private float waitTime;
        public float startWaitTime;
    
    

    public void Start()
    {
        waitTime = startWaitTime;
        randomSpot = Random.Range(0, moveSpots.Length);
    }

    public void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, moveSpots[randomSpot].position, speed * Time.deltaTime);

        if(Vector2.Distance(transform.position, moveSpots[randomSpot].position) < 2.0f)
        {
            
            if(waitTime <= 0) { randomSpot = Random.Range(0, moveSpots.Length); waitTime = startWaitTime; }
            else
            {
                waitTime -= Time.deltaTime;
            }
        }

    }



}
